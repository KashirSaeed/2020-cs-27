﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace DB2020_CS_27
{
    public partial class delete_And_Update : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        Regex regex3 = new Regex("^[a-z A-z 0-9]*$");
        Regex regex4 = new Regex("[a-z0-9!#$%&'+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'+/=?^_`{|}~-]+)@(?:[a-z0-9](?:[a-z0-9-][a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");


        public delete_And_Update()
        {
            InitializeComponent();
        }
        string nums;
        private void getRecord_Click(object sender, EventArgs e)
        {
            //var con = Configuration.getInstance().getConnection();
            //SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE id = @searchID ", con);
            //cmd.Parameters.AddWithValue("@searchID", searchID.Text);
            //cmd.ExecuteNonQuery();


            //var con1 = Configuration.getInstance().getConnection();
            //SqlCommand cmd1 = new SqlCommand("SELECT * FROM Student WHERE RegistrationNumber = @searchID ", con1);
            //cmd1.Parameters.AddWithValue("@searchID", searchID.Text);

            //SqlDataAdapter da = new SqlDataAdapter(cmd1);
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            //update_And_Delete_Table.DataSource = dt;

            //update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            //MessageBox.Show("Successfully displayed");

            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());

            MessageBox.Show(nums);
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM Student WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(nums));

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");
        }

        private void updateStudent_Click(object sender, EventArgs e)
        {
            if (regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString()) && regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString()) && regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString()  ) && regex3.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[5].FormattedValue.ToString())  )
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(" UPDATE Student SET FirstName = @studentID  ,LastName = @studentName , Contact = @courseName , Email = @marks  , RegistrationNumber = @grade , Status = @section  where  Id = @searchID  ", con);
                cmd.Parameters.AddWithValue("@studentID", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@studentName", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@courseName", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@marks", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[4].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@grade", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[5].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@section", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[6].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));


                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
            }
            else
            {
                MessageBox.Show("please update in correct format");
            }


 
        }

        private void deleteStudent_Click(object sender, EventArgs e)
        {


            var con = Configuration.getInstance().getConnection();


            SqlCommand cmd1 = new SqlCommand(" DELETE from StudentAttendance where  StudentId = @searchID  ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd1.ExecuteNonQuery();


            SqlCommand cmd2 = new SqlCommand(" DELETE from StudentResult where  StudentId = @searchID  ", con);
            cmd2.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd2.ExecuteNonQuery();

            SqlCommand cmd3 = new SqlCommand(" DELETE from Student where  Id = @searchID  ", con);
            cmd3.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd3.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }
        
        private void delete_And_Update_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT  Id FROM Student   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();
            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());
            SqlCommand cmd1 = new SqlCommand("SELECT RegistrationNumber FROM Student WHERE Id =@searchID ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(nums));
            SqlDataReader sdr = cmd1.ExecuteReader();
            sdr.Read();
            value.Text = sdr["RegistrationNumber"].ToString();
            sdr.Close();
          
        }
    }
}
