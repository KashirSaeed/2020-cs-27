﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{
    public partial class deleteAndUpdateStudentAttendence : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        public deleteAndUpdateStudentAttendence()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        public void loadindStudentID()
        {
            comboBox2.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand(" SELECT Id  FROM Student WHERE Student.Id  IN ( SELECT StudentId FROM StudentAttendance WHERE AttendanceId = @searchID  )   ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
            SqlDataReader DR2 = cmd1.ExecuteReader();

            while (DR2.Read())
            {
                comboBox2.Items.Add(DR2[0]);

            }
            DR2.Close();
        }

        private void deleteAssessment_Click(object sender, EventArgs e)
        {

        }

        private void deleteAndUpdateStudentAttendence_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("   SELECT Id  FROM ClassAttendance   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();

           
        }
        string num1;
        string num2;
        private void getRecord_Click(object sender, EventArgs e)
        {
            num1 = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());
            num2 = new String(comboBox2.Text.Where(Char.IsDigit).ToArray());


            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM StudentAttendance WHERE AttendanceId =@searchID1 and StudentId = @searchID2 ", con1);
            cmd1.Parameters.AddWithValue("@searchID1", int.Parse(num1));
            cmd1.Parameters.AddWithValue("@searchID2", int.Parse(num2));


            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["AttendanceId"].ReadOnly = true;
            update_And_Delete_Table.Columns["StudentId"].ReadOnly = true;

            MessageBox.Show("Successfully displayed");
        }
        string tempStatus;
        private void updateAssessment_Click(object sender, EventArgs e)
        {
             tempStatus = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString();
            if (tempStatus == "1" || tempStatus == "2" || tempStatus == "3" || tempStatus == "4")
            {
                warningLable.Hide();
                if (regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString()))
                {
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" UPDATE StudentAttendance SET AttendanceStatus = @attendenceStatus  WHERE AttendanceId =@searchID1 and StudentId = @searchID2  ", con);
                    cmd.Parameters.AddWithValue("@attendenceStatus", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                    cmd.Parameters.AddWithValue("@searchID1", int.Parse(num1));
                    cmd.Parameters.AddWithValue("@searchID2", int.Parse(num2));

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Successfully updated");

                }
                else
                {
                    MessageBox.Show("please update in correct format");
                }
            }
            else
            {
                warningLable.Show();

            }
        }

        private void deleteAssessment_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand(" DELETE from StudentAttendance where  AttendanceId = @searchID1 AND StudentId = @searchID2  ", con);
            cmd3.Parameters.AddWithValue("@searchID1", int.Parse(num1));
            cmd3.Parameters.AddWithValue("@searchID2", int.Parse(num2));

            cmd3.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }
        int cbi;
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {

                cbi = (int)comboBox1.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT AttendanceDate  FROM ClassAttendance WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label4.Text = myReader["AttendanceDate"].ToString();

                }
                myReader.Close();
                loadindStudentID();

            }
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            int cbi = (int)comboBox2.SelectedItem;
            var con1 = Configuration.getInstance().getConnection();
            SqlDataReader myReader = null;
            SqlCommand cmd1 = new SqlCommand("SELECT RegistrationNumber  FROM Student WHERE Id = @searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", (cbi.ToString()));
            myReader = cmd1.ExecuteReader();
            while (myReader.Read())
            {
                label3.Text = myReader["RegistrationNumber"].ToString();

            }
            myReader.Close();
        }
    }
}
