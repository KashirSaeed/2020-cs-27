﻿
namespace DB2020_CS_27
{
    partial class RubricLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rubricID = new System.Windows.Forms.Label();
            this.rubricDetail = new System.Windows.Forms.Label();
            this.measurmentLevel = new System.Windows.Forms.Label();
            this.rubricidComboBox = new System.Windows.Forms.ComboBox();
            this.rubDetail = new System.Windows.Forms.RichTextBox();
            this.measurmentLevelTextbox = new System.Windows.Forms.TextBox();
            this.addRubricLevel = new System.Windows.Forms.Button();
            this.warningLable = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rubricID
            // 
            this.rubricID.AutoSize = true;
            this.rubricID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricID.Location = new System.Drawing.Point(65, 25);
            this.rubricID.Name = "rubricID";
            this.rubricID.Size = new System.Drawing.Size(112, 29);
            this.rubricID.TabIndex = 8;
            this.rubricID.Text = "Rubric ID";
            // 
            // rubricDetail
            // 
            this.rubricDetail.AutoSize = true;
            this.rubricDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricDetail.Location = new System.Drawing.Point(65, 146);
            this.rubricDetail.Name = "rubricDetail";
            this.rubricDetail.Size = new System.Drawing.Size(75, 29);
            this.rubricDetail.TabIndex = 7;
            this.rubricDetail.Text = "Detail";
            // 
            // measurmentLevel
            // 
            this.measurmentLevel.AutoSize = true;
            this.measurmentLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.measurmentLevel.Location = new System.Drawing.Point(65, 286);
            this.measurmentLevel.Name = "measurmentLevel";
            this.measurmentLevel.Size = new System.Drawing.Size(210, 29);
            this.measurmentLevel.TabIndex = 6;
            this.measurmentLevel.Text = "Measurment Level";
            // 
            // rubricidComboBox
            // 
            this.rubricidComboBox.FormattingEnabled = true;
            this.rubricidComboBox.Location = new System.Drawing.Point(298, 30);
            this.rubricidComboBox.Name = "rubricidComboBox";
            this.rubricidComboBox.Size = new System.Drawing.Size(438, 24);
            this.rubricidComboBox.TabIndex = 9;
            this.rubricidComboBox.SelectedIndexChanged += new System.EventHandler(this.rubricidComboBox_SelectedIndexChanged);
            this.rubricidComboBox.SelectedValueChanged += new System.EventHandler(this.rubricidComboBox_SelectedValueChanged);
            // 
            // rubDetail
            // 
            this.rubDetail.Location = new System.Drawing.Point(298, 121);
            this.rubDetail.Name = "rubDetail";
            this.rubDetail.Size = new System.Drawing.Size(438, 96);
            this.rubDetail.TabIndex = 10;
            this.rubDetail.Text = "";
            this.rubDetail.TextChanged += new System.EventHandler(this.rubDetail_TextChanged);
            // 
            // measurmentLevelTextbox
            // 
            this.measurmentLevelTextbox.Location = new System.Drawing.Point(298, 286);
            this.measurmentLevelTextbox.Name = "measurmentLevelTextbox";
            this.measurmentLevelTextbox.Size = new System.Drawing.Size(438, 22);
            this.measurmentLevelTextbox.TabIndex = 11;
            // 
            // addRubricLevel
            // 
            this.addRubricLevel.Location = new System.Drawing.Point(592, 383);
            this.addRubricLevel.Name = "addRubricLevel";
            this.addRubricLevel.Size = new System.Drawing.Size(144, 55);
            this.addRubricLevel.TabIndex = 12;
            this.addRubricLevel.Text = "Add Rubric Level";
            this.addRubricLevel.UseVisualStyleBackColor = true;
            this.addRubricLevel.Click += new System.EventHandler(this.addRubricLevel_Click);
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(399, 337);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(195, 17);
            this.warningLable.TabIndex = 22;
            this.warningLable.Text = "Please enter in correct format";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(834, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 29);
            this.label1.TabIndex = 23;
            this.label1.Text = "Rubric Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1090, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 29);
            this.label2.TabIndex = 24;
            this.label2.Text = "lable";
            // 
            // RubricLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.addRubricLevel);
            this.Controls.Add(this.measurmentLevelTextbox);
            this.Controls.Add(this.rubDetail);
            this.Controls.Add(this.rubricidComboBox);
            this.Controls.Add(this.rubricID);
            this.Controls.Add(this.rubricDetail);
            this.Controls.Add(this.measurmentLevel);
            this.Name = "RubricLevel";
            this.Text = "RubricLevel";
            this.Load += new System.EventHandler(this.RubricLevel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label rubricID;
        private System.Windows.Forms.Label rubricDetail;
        private System.Windows.Forms.Label measurmentLevel;
        private System.Windows.Forms.ComboBox rubricidComboBox;
        private System.Windows.Forms.RichTextBox rubDetail;
        private System.Windows.Forms.TextBox measurmentLevelTextbox;
        private System.Windows.Forms.Button addRubricLevel;
        private System.Windows.Forms.Label warningLable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}