﻿
namespace DB2020_CS_27
{
    partial class studentResultDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.disStudentResult = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.disStudentResult)).BeginInit();
            this.SuspendLayout();
            // 
            // disStudentResult
            // 
            this.disStudentResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.disStudentResult.Location = new System.Drawing.Point(48, 91);
            this.disStudentResult.Name = "disStudentResult";
            this.disStudentResult.RowHeadersWidth = 51;
            this.disStudentResult.RowTemplate.Height = 24;
            this.disStudentResult.Size = new System.Drawing.Size(704, 269);
            this.disStudentResult.TabIndex = 2;
            // 
            // studentResultDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.disStudentResult);
            this.Name = "studentResultDisplay";
            this.Text = "studentResultDisplay";
            this.Load += new System.EventHandler(this.studentResultDisplay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.disStudentResult)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView disStudentResult;
    }
}