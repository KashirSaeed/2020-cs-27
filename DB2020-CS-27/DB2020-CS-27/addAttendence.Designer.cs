﻿
namespace DB2020_CS_27
{
    partial class addAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attendenceDate = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.addDate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // attendenceDate
            // 
            this.attendenceDate.AutoSize = true;
            this.attendenceDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendenceDate.Location = new System.Drawing.Point(81, 91);
            this.attendenceDate.Name = "attendenceDate";
            this.attendenceDate.Size = new System.Drawing.Size(190, 29);
            this.attendenceDate.TabIndex = 0;
            this.attendenceDate.Text = "Attendence Date";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(351, 96);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(336, 22);
            this.dateTimePicker1.TabIndex = 1;
            // 
            // addDate
            // 
            this.addDate.Location = new System.Drawing.Point(513, 156);
            this.addDate.Name = "addDate";
            this.addDate.Size = new System.Drawing.Size(174, 38);
            this.addDate.TabIndex = 2;
            this.addDate.Text = "Add Attendence Date";
            this.addDate.UseVisualStyleBackColor = true;
            this.addDate.Click += new System.EventHandler(this.addDate_Click);
            // 
            // addAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.addDate);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.attendenceDate);
            this.Name = "addAttendence";
            this.Text = "Class_Attendence";
            this.Load += new System.EventHandler(this.addAttendence_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label attendenceDate;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button addDate;
    }
}