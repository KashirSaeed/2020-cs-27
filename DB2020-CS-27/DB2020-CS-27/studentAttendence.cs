﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace DB2020_CS_27
{
    public partial class studentAttendence : Form
    {
       
        public studentAttendence()
        {
            InitializeComponent();
        }

        public void loadingAttendenceID()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT Id FROM ClassAttendance   ", con);
            SqlDataReader DR = cmd.ExecuteReader();

            while (DR.Read())
            {
                attendenceIDCombobox.Items.Add(DR[0]);
            }
            

            DR.Close();
        }
        public void loadindStudentID()
        {
            studentIDCombobox.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT Id FROM Student EXCEPT( SELECT StudentId FROM StudentAttendance WHERE AttendanceId = @searchID  )  ", con);
            cmd.Parameters.AddWithValue("@searchID", int.Parse(attendenceIDCombobox.Text));
            SqlDataReader DR1 = cmd.ExecuteReader();
            while (DR1.Read())
            {
                studentIDCombobox.Items.Add(DR1[0]);
            }
            DR1.Close();
        }

        public void loadStudentData()
        {
            //var con = Configuration.getInstance().getConnection();
            //SqlCommand cmd = new SqlCommand(" select Id , RegistrationNumber from Student ", con);
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            //dataGridView1.DataSource = dt;

            //DataTable dtEmp = new DataTable();
            //// add column to datatable  
            //dt.Columns.Add("Mark Attendance", typeof(RadioButton) );
            ////dt.Columns.Add("EmpID", typeof(int));
            ////dt.Columns.Add("EmpName", typeof(string));
            ////dt.Columns.Add("EmpCity", typeof(string));
        }

        private void studentAttendence_Load(object sender, EventArgs e)
        {

            loadingAttendenceID();
            //loadindStudentID();
            loadStudentData();
        }



        private void attendenceIDCombobox_SelectedValueChanged(object sender, EventArgs e)
        {

            if (attendenceIDCombobox.SelectedItem != null)
            {
                int cbi = (int)attendenceIDCombobox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT AttendanceDate  FROM ClassAttendance WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    idValue.Text = myReader["AttendanceDate"].ToString();

                }
                myReader.Close();
                loadindStudentID();

            }

        }

        private void attendence_Click(object sender, EventArgs e)
        {

            





            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" insert into StudentAttendance values(@idValue , @stuID  , @statusOfAttendence   ) ", con);
            cmd.Parameters.AddWithValue("@idValue", int.Parse(attendenceIDCombobox.Text));
            cmd.Parameters.AddWithValue("@stuID", int.Parse(studentIDCombobox.Text));
            cmd.Parameters.AddWithValue("@statusOfAttendence", int.Parse(statusOfAttendence.Text));
            
            cmd.ExecuteNonQuery();
            MessageBox.Show("Student Added Successfully ");


            //var con1 = Configuration.getInstance().getConnection();
            //SqlCommand cmd1 = new SqlCommand(" insert into Lookup values(@Attendencemarked1 , @category2   ) ", con1);
            //cmd1.Parameters.AddWithValue("@Attendencemarked1", (Attendencemarked));
            //cmd1.Parameters.AddWithValue("@category2", (category1));
            //cmd1.ExecuteNonQuery();
            //MessageBox.Show("Student Added Successfully ");

        }

        string category1;
        string Attendencemarked;

        private void attendenceStatusCombo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (attendenceStatusCombo.SelectedItem != null)
            {
                //string cbi = (string)attendenceStatusCombo.SelectedItem;
                //if(cbi == "Present")
                //{
                //    statusOfAttendence.Text = "1";
                //}

                //if (cbi == "Absent")
                //{
                //    statusOfAttendence.Text = "2";

                //}
                //if (cbi == "Leave")
                //{
                //    statusOfAttendence.Text = "3";

                //}
                //if (cbi == "Late")
                //{
                //    statusOfAttendence.Text = "4";

                //}

                //var con1 = Configuration.getInstance().getConnection();
                //SqlDataReader myReader = null;
                //SqlCommand cmd1 = new SqlCommand("SELECT Id  FROM ClassAttendance WHERE AttendanceDate = @searchID ", con1);
                //cmd1.Parameters.AddWithValue("@searchID", DateTime.Parse(cbi.ToString()));
                //myReader = cmd1.ExecuteReader();
                //while (myReader.Read())
                //{
                //    idValue.Text = myReader["Id"].ToString();

                //}
                //myReader.Close();

                if (attendenceStatusCombo.SelectedItem != null)
                {
                    string cbi = (string)attendenceStatusCombo.SelectedItem;

                    if (cbi == "Present")
                    {
                         Attendencemarked = "Present";
                        statusOfAttendence.Text = "1";
                        category1 = "ATTENDANCE_STATUS";
                    }

                    if (cbi == "Absent")
                    {
                        Attendencemarked = "Absent";
                        statusOfAttendence.Text = "2";
                        category1 = "ATTENDANCE_STATUS";


                    }
                    if (cbi == "Leave")
                    {
                        Attendencemarked = "Leave";
                        statusOfAttendence.Text = "3";
                        category1 = "ATTENDANCE_STATUS";


                    }
                    if (cbi == "Late")
                    {
                        Attendencemarked = "Late";
                        statusOfAttendence.Text = "4";
                        category1 = "ATTENDANCE_STATUS";


                    }

                    //var con1 = Configuration.getInstance().getConnection();
                    //SqlDataReader myReader = null;
                    //SqlCommand cmd1 = new SqlCommand("SELECT Id  FROM ClassAttendance WHERE AttendanceDate = @searchID ", con1);
                    //cmd1.Parameters.AddWithValue("@searchID", DateTime.Parse(cbi.ToString()));
                    //myReader = cmd1.ExecuteReader();
                    //while (myReader.Read())
                    //{
                    //    idValue.Text = myReader["Id"].ToString();

                    //}
                    //myReader.Close();


                }


            }
        }

        private void idValue_Click(object sender, EventArgs e)
        {

        }

        private void studentIDCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (studentIDCombobox.SelectedItem != null)
            //{
                int cbi = (int)studentIDCombobox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT RegistrationNumber  FROM Student WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID",(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    stuID.Text = myReader["RegistrationNumber"].ToString();

                }
                myReader.Close();


            //}
        }

        internal class ComboBoxItem
        {
            public object Content { get; internal set; }
        }

        internal class SelectionChangedEventArgs
        {
        }

        private void attendenceIDCombobox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}