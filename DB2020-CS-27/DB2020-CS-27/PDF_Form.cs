﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.SqlClient;


namespace DB2020_CS_27
{
    public partial class PDF_Form : Form
    {
        public PDF_Form()
        {
            InitializeComponent();
        }

        private void PDF_Form_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string data = "University of Engineering and Technology,Lahore";
            var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 25, iTextSharp.text.Font.BOLD));
            p1.Alignment = Element.ALIGN_CENTER;

            var con = Configuration.getInstance().getConnection();
            Document dc = new Document();


            if (comboBox1.Text == "CLO Wise Result")
            {

                SqlCommand cmd = new SqlCommand(" SELECT  Clo.Name, Student.RegistrationNumber  , SUM(Assessment.TotalMarks) AS CLO_Total_Marks  ,  SUM(cast((cast(RubricLevel.MeasurementLevel as float) / cast(R.Maximum_Rubric_Level as float )) as float ) * AssessmentComponent.TotalMarks)  AS Obtained_Marks FROM (SELECT  Assessment.Id AS Assessment_ID , AssessmentComponent.Id as Assessment_Component_ID , MAX(RubricLevel.MeasurementLevel) AS Maximum_Rubric_Level   FROM Assessment JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId JOIN Rubric ON Rubric.Id = AssessmentComponent.RubricId JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId JOIN StudentResult ON  StudentResult.AssessmentComponentId = AssessmentComponent.Id GROUP BY Assessment.Id , AssessmentComponent.Id ) AS R JOIN StudentResult ON StudentResult.AssessmentComponentId = R.Assessment_Component_ID JOIN RubricLevel ON StudentResult.RubricMeasurementId = RubricLevel.Id join AssessmentComponent on AssessmentComponent.Id = StudentResult.AssessmentComponentId JOIN Student ON Student.Id = StudentResult.StudentId JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId join Rubric on Rubric.Id = RubricLevel.RubricId join Clo on Clo.Id = Rubric.CloId group by Clo.Name, Student.RegistrationNumber  order by clo.Name", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                //dc = new Document();
                PdfWriter.GetInstance(dc, new FileStream("D:/Computer Science Data/Forth Semester/Data Bases Lab/Mid Term Project/2020-cs-27/DB2020-CS-27/CLO_Wise_Result.pdf", FileMode.Create));
                 //table = new PdfPTable(dataGridView1.Columns.Count);

            }
            if (comboBox1.Text == "Assessment Wise Result")
            {

                SqlCommand cmd = new SqlCommand(" SELECT  Student.RegistrationNumber , Assessment.Title ,  MAX(Assessment.TotalMarks) as Total_Marks ,  SUM(cast((cast(RubricLevel.MeasurementLevel as float) / cast(R.Maximum_Rubric_Level as float )) as float ) * AssessmentComponent.TotalMarks) AS Obtained_MArks   FROM (SELECT  Assessment.Id AS Assessment_ID , AssessmentComponent.Id as Assessment_Component_ID , MAX(RubricLevel.MeasurementLevel) AS Maximum_Rubric_Level   FROM Assessment JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId JOIN Rubric ON Rubric.Id = AssessmentComponent.RubricId JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId JOIN StudentResult ON  StudentResult.AssessmentComponentId = AssessmentComponent.Id GROUP BY Assessment.Id , AssessmentComponent.Id ) AS R JOIN StudentResult ON StudentResult.AssessmentComponentId = R.Assessment_Component_ID JOIN RubricLevel ON StudentResult.RubricMeasurementId = RubricLevel.Id join AssessmentComponent on AssessmentComponent.Id = StudentResult.AssessmentComponentId JOIN Student ON Student.Id = StudentResult.StudentId JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId group by Student.RegistrationNumber , Assessment.Title", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                 //dc = new Document();
                PdfWriter.GetInstance(dc, new FileStream("D:/Computer Science Data/Forth Semester/Data Bases Lab/Mid Term Project/2020-cs-27/DB2020-CS-27/Assessment_Wise_Result.pdf", FileMode.Create));
                 //table = new PdfPTable(dataGridView1.Columns.Count);

            }
            if (comboBox1.Text == "Assessment Component Wise Result")
            {

                SqlCommand cmd = new SqlCommand("SELECT Student.RegistrationNumber , Assessment.Title ,  AssessmentComponent.Name , R.Maximum_Rubric_Level ,  (cast((cast(RubricLevel.MeasurementLevel as float) / cast(R.Maximum_Rubric_Level as float)) as float) * AssessmentComponent.TotalMarks) AS Obtained_MArks   FROM(SELECT  Assessment.Id AS Assessment_ID, AssessmentComponent.Id as Assessment_Component_ID, MAX(RubricLevel.MeasurementLevel) AS Maximum_Rubric_Level   FROM Assessment JOIN AssessmentComponent ON Assessment.Id = AssessmentComponent.AssessmentId JOIN Rubric ON Rubric.Id = AssessmentComponent.RubricId JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId JOIN StudentResult ON  StudentResult.AssessmentComponentId = AssessmentComponent.Id GROUP BY Assessment.Id, AssessmentComponent.Id) AS R JOIN StudentResult ON StudentResult.AssessmentComponentId = R.Assessment_Component_ID JOIN RubricLevel ON StudentResult.RubricMeasurementId = RubricLevel.Id join AssessmentComponent on AssessmentComponent.Id = StudentResult.AssessmentComponentId JOIN Student ON Student.Id = StudentResult.StudentId JOIN Assessment ON Assessment.Id = AssessmentComponent.AssessmentId ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                 //dc = new Document();
                PdfWriter.GetInstance(dc, new FileStream("D:/Computer Science Data/Forth Semester/Data Bases Lab/Mid Term Project/2020-cs-27/DB2020-CS-27/Assessment_Component_Wise_Result.pdf", FileMode.Create));
                 //table = new PdfPTable(dataGridView1.Columns.Count);


            }
            if (comboBox1.Text == "Date Wise Attendence")
            {

                SqlCommand cmd = new SqlCommand(" SELECT R1.Id , R1.AttendanceDate , R2.Total_Students , R1.Present_Students ,CAST((CAST(R1.Present_Students AS float)   / CAST(R2.Total_Students AS float)  ) *100 AS float )     AS Attendance_Percentage FROM (SELECT Id , AttendanceDate , COUNT(Id) AS Present_Students FROM ClassAttendance JOIN StudentAttendance ON ClassAttendance.Id = StudentAttendance.AttendanceId GROUP BY Id , AttendanceDate ) AS R1 CROSS JOIN  (SELECT COUNT(Student.Id) AS Total_Students FROM Student) AS R2", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                 //dc = new Document();
                PdfWriter.GetInstance(dc, new FileStream("D:/Computer Science Data/Forth Semester/Data Bases Lab/Mid Term Project/2020-cs-27/DB2020-CS-27/Date_Wise_Attendence.pdf", FileMode.Create));
                 //table = new PdfPTable(dataGridView1.Columns.Count);

            }
            if (comboBox1.Text == "Student Wise Attendence")
            {

                SqlCommand cmd = new SqlCommand("SELECT Student.RegistrationNumber , MAX(R.Total_Days) AS Total_Numbers_of_Days , COUNT(StudentId) as Number_of_Days_Present , (CAST(CAST(COUNT(StudentId) AS float) / CAST(MAX(R.Total_Days) AS float) AS float)*100) AS Present_Percentage                   FROM  (SELECT   count(DISTINCT AttendanceDate) AS Total_Days FROM ClassAttendance JOIN StudentAttendance  ON ClassAttendance.Id = StudentAttendance.AttendanceId) as R CROSS JOIN ClassAttendance JOIN StudentAttendance  ON ClassAttendance.Id = StudentAttendance.AttendanceId JOIN Student ON Student.Id = StudentAttendance.StudentId WHERE StudentAttendance.AttendanceStatus = '1' GROUP BY Student.RegistrationNumber ", con);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;

                 //dc = new Document();
                 PdfWriter.GetInstance(dc, new FileStream("D:/Computer Science Data/Forth Semester/Data Bases Lab/Mid Term Project/2020-cs-27/DB2020-CS-27/Student_Wise_Attendence.pdf", FileMode.Create));
                 //table = new PdfPTable(dataGridView1.Columns.Count);

            }



            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);







            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.Padding = 6;
            table.DefaultCell.PaddingTop = 10;

            table.WidthPercentage = 100;
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                    }
                }
            }
            dc.Open();
            dc.Add(p1);
            dc.Add(table);
            dc.Close();
            MessageBox.Show("PDF is created");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
