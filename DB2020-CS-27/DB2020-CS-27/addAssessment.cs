﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace DB2020_CS_27
{
    public partial class addAssessment : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[a-z A-z0-9]*$");
        Regex regex3 = new Regex("^[0-9]*$");
        Regex regex4 = new Regex("[a-z0-9!#$%&'+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'+/=?^_`{|}~-]+)@(?:[a-z0-9](?:[a-z0-9-][a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

        public addAssessment()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        private void addAssessmentButton_Click(object sender, EventArgs e)
        {
            if (regex1.IsMatch(textBox1.Text) && regex3.IsMatch(textBox3.Text) && regex3.IsMatch(textBox4.Text))
            {

                if (textBox1.Text == "" || textBox3.Text == "" || textBox4.Text == "")
                {
                    warningLable.Show();

                }
                else
                {

                    warningLable.Hide();
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" insert into Assessment values(@Title , @DateCreated , @TotalMarks , @TotalWeightage   ) ", con);
                    cmd.Parameters.AddWithValue("@Title", textBox1.Text);
                    cmd.Parameters.AddWithValue("@DateCreated", DateTime.Parse(dateTimePicker1.Text));
                    cmd.Parameters.AddWithValue("@TotalMarks", int.Parse(textBox3.Text));
                    cmd.Parameters.AddWithValue("@TotalWeightage", int.Parse((textBox4.Text)));


                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Student Added Successfully ");

                }
            }
            else
            {
                MessageBox.Show(warningLable.Text);

            }
        }
        

    private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void totalWeightage_Click(object sender, EventArgs e)
        {

        }

        private void totalMarks_Click(object sender, EventArgs e)
        {

        }

        private void dateCreated_Click(object sender, EventArgs e)
        {

        }

        private void tittle_Click(object sender, EventArgs e)
        {

        }

        private void addAssessment_Load(object sender, EventArgs e)
        {

        }
    }
}
