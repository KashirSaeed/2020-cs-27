﻿
namespace DB2020_CS_27
{
    partial class addAssessment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tittle = new System.Windows.Forms.Label();
            this.dateCreated = new System.Windows.Forms.Label();
            this.totalMarks = new System.Windows.Forms.Label();
            this.totalWeightage = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.addAssessmentButton = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.warningLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tittle
            // 
            this.tittle.AutoSize = true;
            this.tittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tittle.Location = new System.Drawing.Point(131, 32);
            this.tittle.Name = "tittle";
            this.tittle.Size = new System.Drawing.Size(67, 29);
            this.tittle.TabIndex = 0;
            this.tittle.Text = "Tittle";
            this.tittle.Click += new System.EventHandler(this.tittle_Click);
            // 
            // dateCreated
            // 
            this.dateCreated.AutoSize = true;
            this.dateCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateCreated.Location = new System.Drawing.Point(131, 86);
            this.dateCreated.Name = "dateCreated";
            this.dateCreated.Size = new System.Drawing.Size(155, 29);
            this.dateCreated.TabIndex = 1;
            this.dateCreated.Text = "Date Created";
            this.dateCreated.Click += new System.EventHandler(this.dateCreated_Click);
            // 
            // totalMarks
            // 
            this.totalMarks.AutoSize = true;
            this.totalMarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalMarks.Location = new System.Drawing.Point(131, 142);
            this.totalMarks.Name = "totalMarks";
            this.totalMarks.Size = new System.Drawing.Size(139, 29);
            this.totalMarks.TabIndex = 2;
            this.totalMarks.Text = "Total Marks";
            this.totalMarks.Click += new System.EventHandler(this.totalMarks_Click);
            // 
            // totalWeightage
            // 
            this.totalWeightage.AutoSize = true;
            this.totalWeightage.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalWeightage.Location = new System.Drawing.Point(131, 200);
            this.totalWeightage.Name = "totalWeightage";
            this.totalWeightage.Size = new System.Drawing.Size(190, 29);
            this.totalWeightage.TabIndex = 3;
            this.totalWeightage.Text = "Total Weightage";
            this.totalWeightage.Click += new System.EventHandler(this.totalWeightage_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(409, 39);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(273, 22);
            this.textBox1.TabIndex = 5;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(409, 144);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(273, 22);
            this.textBox3.TabIndex = 7;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(409, 207);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(273, 22);
            this.textBox4.TabIndex = 8;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // addAssessmentButton
            // 
            this.addAssessmentButton.Location = new System.Drawing.Point(510, 290);
            this.addAssessmentButton.Name = "addAssessmentButton";
            this.addAssessmentButton.Size = new System.Drawing.Size(172, 51);
            this.addAssessmentButton.TabIndex = 9;
            this.addAssessmentButton.Text = "Add Assessment";
            this.addAssessmentButton.UseVisualStyleBackColor = true;
            this.addAssessmentButton.Click += new System.EventHandler(this.addAssessmentButton_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(409, 86);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(273, 22);
            this.dateTimePicker1.TabIndex = 10;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(432, 255);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(195, 17);
            this.warningLable.TabIndex = 16;
            this.warningLable.Text = "Please enter in correct format";
            // 
            // addAssessment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 495);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.addAssessmentButton);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.totalWeightage);
            this.Controls.Add(this.totalMarks);
            this.Controls.Add(this.dateCreated);
            this.Controls.Add(this.tittle);
            this.Name = "addAssessment";
            this.Text = "addAssessment";
            this.Load += new System.EventHandler(this.addAssessment_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tittle;
        private System.Windows.Forms.Label dateCreated;
        private System.Windows.Forms.Label totalMarks;
        private System.Windows.Forms.Label totalWeightage;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button addAssessmentButton;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label warningLable;
    }
}