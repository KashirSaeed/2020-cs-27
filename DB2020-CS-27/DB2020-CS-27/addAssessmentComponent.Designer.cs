﻿
namespace DB2020_CS_27
{
    partial class addAssessmentComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rubricID = new System.Windows.Forms.Label();
            this.dateUpdated = new System.Windows.Forms.Label();
            this.assessmentID = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.Label();
            this.dateCreated = new System.Windows.Forms.Label();
            this.totalMarks = new System.Windows.Forms.Label();
            this.measurmentLevelTextbox = new System.Windows.Forms.TextBox();
            this.totalMarksBox = new System.Windows.Forms.TextBox();
            this.rubricIDCombobox = new System.Windows.Forms.ComboBox();
            this.assessmentIDCombobox = new System.Windows.Forms.ComboBox();
            this.addAssessComponent = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.warningLable = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // rubricID
            // 
            this.rubricID.AutoSize = true;
            this.rubricID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricID.Location = new System.Drawing.Point(77, 75);
            this.rubricID.Name = "rubricID";
            this.rubricID.Size = new System.Drawing.Size(112, 29);
            this.rubricID.TabIndex = 11;
            this.rubricID.Text = "Rubric ID";
            // 
            // dateUpdated
            // 
            this.dateUpdated.AutoSize = true;
            this.dateUpdated.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateUpdated.Location = new System.Drawing.Point(77, 284);
            this.dateUpdated.Name = "dateUpdated";
            this.dateUpdated.Size = new System.Drawing.Size(161, 29);
            this.dateUpdated.TabIndex = 10;
            this.dateUpdated.Text = "Date Updated";
            // 
            // assessmentID
            // 
            this.assessmentID.AutoSize = true;
            this.assessmentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentID.Location = new System.Drawing.Point(77, 354);
            this.assessmentID.Name = "assessmentID";
            this.assessmentID.Size = new System.Drawing.Size(172, 29);
            this.assessmentID.TabIndex = 9;
            this.assessmentID.Text = "Assessment ID";
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.Location = new System.Drawing.Point(77, 16);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(345, 29);
            this.name.TabIndex = 14;
            this.name.Text = "Assessment Component Name";
            // 
            // dateCreated
            // 
            this.dateCreated.AutoSize = true;
            this.dateCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateCreated.Location = new System.Drawing.Point(77, 214);
            this.dateCreated.Name = "dateCreated";
            this.dateCreated.Size = new System.Drawing.Size(155, 29);
            this.dateCreated.TabIndex = 13;
            this.dateCreated.Text = "Date Created";
            // 
            // totalMarks
            // 
            this.totalMarks.AutoSize = true;
            this.totalMarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalMarks.Location = new System.Drawing.Point(77, 150);
            this.totalMarks.Name = "totalMarks";
            this.totalMarks.Size = new System.Drawing.Size(139, 29);
            this.totalMarks.TabIndex = 12;
            this.totalMarks.Text = "Total Marks";
            // 
            // measurmentLevelTextbox
            // 
            this.measurmentLevelTextbox.Location = new System.Drawing.Point(462, 12);
            this.measurmentLevelTextbox.Name = "measurmentLevelTextbox";
            this.measurmentLevelTextbox.Size = new System.Drawing.Size(438, 22);
            this.measurmentLevelTextbox.TabIndex = 15;
            // 
            // totalMarksBox
            // 
            this.totalMarksBox.Location = new System.Drawing.Point(462, 150);
            this.totalMarksBox.Name = "totalMarksBox";
            this.totalMarksBox.Size = new System.Drawing.Size(438, 22);
            this.totalMarksBox.TabIndex = 17;
            // 
            // rubricIDCombobox
            // 
            this.rubricIDCombobox.FormattingEnabled = true;
            this.rubricIDCombobox.Location = new System.Drawing.Point(462, 80);
            this.rubricIDCombobox.Name = "rubricIDCombobox";
            this.rubricIDCombobox.Size = new System.Drawing.Size(438, 24);
            this.rubricIDCombobox.TabIndex = 20;
            this.rubricIDCombobox.SelectedValueChanged += new System.EventHandler(this.rubricIDCombobox_SelectedValueChanged);
            // 
            // assessmentIDCombobox
            // 
            this.assessmentIDCombobox.FormattingEnabled = true;
            this.assessmentIDCombobox.Location = new System.Drawing.Point(462, 354);
            this.assessmentIDCombobox.Name = "assessmentIDCombobox";
            this.assessmentIDCombobox.Size = new System.Drawing.Size(438, 24);
            this.assessmentIDCombobox.TabIndex = 21;
            this.assessmentIDCombobox.SelectedValueChanged += new System.EventHandler(this.assessmentIDCombobox_SelectedValueChanged);
            // 
            // addAssessComponent
            // 
            this.addAssessComponent.Location = new System.Drawing.Point(718, 457);
            this.addAssessComponent.Name = "addAssessComponent";
            this.addAssessComponent.Size = new System.Drawing.Size(182, 49);
            this.addAssessComponent.TabIndex = 22;
            this.addAssessComponent.Text = "Add Assessment Component";
            this.addAssessComponent.UseVisualStyleBackColor = true;
            this.addAssessComponent.Click += new System.EventHandler(this.addAssessComponent_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(462, 284);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(438, 22);
            this.dateTimePicker2.TabIndex = 19;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(462, 214);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(438, 22);
            this.dateTimePicker1.TabIndex = 18;
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(441, 407);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(195, 17);
            this.warningLable.TabIndex = 23;
            this.warningLable.Text = "Please enter in correct format";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1131, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 29);
            this.label2.TabIndex = 26;
            this.label2.Text = "lable";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(967, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 29);
            this.label1.TabIndex = 25;
            this.label1.Text = "Rubric Detail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1209, 349);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 29);
            this.label3.TabIndex = 28;
            this.label3.Text = "lable";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(967, 349);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 29);
            this.label4.TabIndex = 27;
            this.label4.Text = "Assessment Tittle";
            // 
            // addAssessmentComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1345, 535);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.addAssessComponent);
            this.Controls.Add(this.assessmentIDCombobox);
            this.Controls.Add(this.rubricIDCombobox);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.totalMarksBox);
            this.Controls.Add(this.measurmentLevelTextbox);
            this.Controls.Add(this.name);
            this.Controls.Add(this.dateCreated);
            this.Controls.Add(this.totalMarks);
            this.Controls.Add(this.rubricID);
            this.Controls.Add(this.dateUpdated);
            this.Controls.Add(this.assessmentID);
            this.Name = "addAssessmentComponent";
            this.Text = "AssessmentComponent";
            this.Load += new System.EventHandler(this.addAssessmentComponent_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label rubricID;
        private System.Windows.Forms.Label dateUpdated;
        private System.Windows.Forms.Label assessmentID;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label dateCreated;
        private System.Windows.Forms.Label totalMarks;
        private System.Windows.Forms.TextBox measurmentLevelTextbox;
        private System.Windows.Forms.TextBox totalMarksBox;
        private System.Windows.Forms.ComboBox rubricIDCombobox;
        private System.Windows.Forms.ComboBox assessmentIDCombobox;
        private System.Windows.Forms.Button addAssessComponent;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label warningLable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}