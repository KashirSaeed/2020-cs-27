﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace DB2020_CS_27
{
    public partial class deleteAndUpdateRubric : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        public deleteAndUpdateRubric()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        private void deleteAndUpdateRubric_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT Id FROM Rubric   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();
        }
        string nums;
        private void getRecord_Click(object sender, EventArgs e)
        {
            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM Rubric WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(nums));

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");
        }
        string tempCloid;
        int flag = 0;
        private void updateAssessment_Click(object sender, EventArgs e)
        {
            tempCloid = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString();


            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("  SELECT Id FROM Clo   ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(tempCloid));
            SqlDataReader DR1 = cmd1.ExecuteReader();

           

            while (DR1.Read())
            {
                flag = 0;
                if(DR1.GetValue(0).ToString() == tempCloid)
                {
                    warningLable.Hide();
                    if (regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString()) && regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString()))
                    {
                        flag = 1;
                        DR1.Close();

                        //var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(" UPDATE Rubric SET Details = @studentID , CloId = @studentName  where  Id = @searchID  ", con);
                        cmd.Parameters.AddWithValue("@studentID", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                        cmd.Parameters.AddWithValue("@studentName", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                        cmd.Parameters.AddWithValue("@searchID", int.Parse(nums));


                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully updated");
                        break;

                    }
                    else
                    {
                        MessageBox.Show("please update in correct format");
                    }


                }
                else
                {
                    warningLable.Show();
                }

            }
            if(flag != 1)
            {
                DR1.Close();
            }
            //DR1.Close();



           
        }

        private void update_And_Delete_Table_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
            MessageBox.Show("Any of the previously edited is incorrect");
        }

        private void deleteAssessment_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd7 = new SqlCommand("DELETE from StudentResult where StudentResult.AssessmentComponentId in ( select StudentResult.AssessmentComponentId from Rubric join AssessmentComponent on Rubric.Id = AssessmentComponent.RubricId join StudentResult on AssessmentComponent.Id = StudentResult.AssessmentComponentId where Rubric.Id = @searchID)  ", con);
            cmd7.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd7.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");

            SqlCommand cmd5 = new SqlCommand(" DELETE FROM StudentResult WHERE StudentResult.RubricMeasurementId IN( SELECT StudentResult.RubricMeasurementId FROM StudentResult JOIN RubricLevel ON StudentResult.RubricMeasurementId = RubricLevel.Id JOIN Rubric ON Rubric.Id = RubricLevel.RubricId WHERE Rubric.Id = @searchID)  ", con);
            cmd5.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd5.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");



            SqlCommand cmd6 = new SqlCommand(" DELETE from RubricLevel where  RubricId = @searchID  ", con);
            cmd6.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd6.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");



            SqlCommand cmd4 = new SqlCommand(" DELETE from AssessmentComponent where  RubricId = @searchID  ", con);
            cmd4.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd4.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");

            SqlCommand cmd3 = new SqlCommand(" DELETE from Rubric where  Id = @searchID  ", con);
            cmd3.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd3.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void rubricID_Click(object sender, EventArgs e)
        {

        }

        private void update_And_Delete_Table_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        int cbi;
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {

                cbi = (int)comboBox1.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Details  FROM Rubric WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label2.Text = myReader["Details"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }
    }
}
