﻿
namespace DB2020_CS_27
{
    partial class deleteAndUpdateClassAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateAttendenceDate = new System.Windows.Forms.Button();
            this.deleteAttendenceDate = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.attendenceID = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.value = new System.Windows.Forms.Label();
            this.attendenceDate = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // updateAttendenceDate
            // 
            this.updateAttendenceDate.Location = new System.Drawing.Point(464, 367);
            this.updateAttendenceDate.Name = "updateAttendenceDate";
            this.updateAttendenceDate.Size = new System.Drawing.Size(150, 53);
            this.updateAttendenceDate.TabIndex = 23;
            this.updateAttendenceDate.Text = "Update Attendence Date";
            this.updateAttendenceDate.UseVisualStyleBackColor = true;
            this.updateAttendenceDate.Click += new System.EventHandler(this.updateAttendenceDate_Click);
            // 
            // deleteAttendenceDate
            // 
            this.deleteAttendenceDate.Location = new System.Drawing.Point(620, 367);
            this.deleteAttendenceDate.Name = "deleteAttendenceDate";
            this.deleteAttendenceDate.Size = new System.Drawing.Size(150, 53);
            this.deleteAttendenceDate.TabIndex = 22;
            this.deleteAttendenceDate.Text = "Delete Attendence Date";
            this.deleteAttendenceDate.UseVisualStyleBackColor = true;
            this.deleteAttendenceDate.Click += new System.EventHandler(this.deleteAttendenceDate_Click);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(35, 84);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 21;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // attendenceID
            // 
            this.attendenceID.AutoSize = true;
            this.attendenceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendenceID.Location = new System.Drawing.Point(30, 31);
            this.attendenceID.Name = "attendenceID";
            this.attendenceID.Size = new System.Drawing.Size(136, 25);
            this.attendenceID.TabIndex = 19;
            this.attendenceID.Text = "Attendence ID";
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(120, 176);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 18;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(300, 31);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 24;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // value
            // 
            this.value.AutoSize = true;
            this.value.Location = new System.Drawing.Point(869, 34);
            this.value.Name = "value";
            this.value.Size = new System.Drawing.Size(46, 17);
            this.value.TabIndex = 27;
            this.value.Text = "label2";
            // 
            // attendenceDate
            // 
            this.attendenceDate.AutoSize = true;
            this.attendenceDate.Location = new System.Drawing.Point(736, 34);
            this.attendenceDate.Name = "attendenceDate";
            this.attendenceDate.Size = new System.Drawing.Size(122, 17);
            this.attendenceDate.TabIndex = 26;
            this.attendenceDate.Text = "Attendence Date: ";
            // 
            // deleteAndUpdateClassAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1049, 450);
            this.Controls.Add(this.value);
            this.Controls.Add(this.attendenceDate);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateAttendenceDate);
            this.Controls.Add(this.deleteAttendenceDate);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.attendenceID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "deleteAndUpdateClassAttendence";
            this.Text = "deleteAndUpdateClassAttendence";
            this.Load += new System.EventHandler(this.deleteAndUpdateClassAttendence_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateAttendenceDate;
        private System.Windows.Forms.Button deleteAttendenceDate;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label attendenceID;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label value;
        private System.Windows.Forms.Label attendenceDate;
    }
}