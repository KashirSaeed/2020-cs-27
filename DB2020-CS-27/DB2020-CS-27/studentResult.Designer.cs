﻿
namespace DB2020_CS_27
{
    partial class studentResult
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.studentID = new System.Windows.Forms.Label();
            this.attendenceComponentID = new System.Windows.Forms.Label();
            this.rubricMeasurnmentID = new System.Windows.Forms.Label();
            this.evaluationDate = new System.Windows.Forms.Label();
            this.studentIDbox = new System.Windows.Forms.ComboBox();
            this.attendenceCompIDBox = new System.Windows.Forms.ComboBox();
            this.rubricMeasurnmentIDBox = new System.Windows.Forms.ComboBox();
            this.addStudentResult = new System.Windows.Forms.Button();
            this.evaluationDatebox = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4S = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // studentID
            // 
            this.studentID.AutoSize = true;
            this.studentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentID.Location = new System.Drawing.Point(66, 83);
            this.studentID.Name = "studentID";
            this.studentID.Size = new System.Drawing.Size(124, 29);
            this.studentID.TabIndex = 1;
            this.studentID.Text = "Student ID";
            this.studentID.Click += new System.EventHandler(this.attendenceDate_Click);
            // 
            // attendenceComponentID
            // 
            this.attendenceComponentID.AutoSize = true;
            this.attendenceComponentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendenceComponentID.Location = new System.Drawing.Point(66, 153);
            this.attendenceComponentID.Name = "attendenceComponentID";
            this.attendenceComponentID.Size = new System.Drawing.Size(294, 29);
            this.attendenceComponentID.TabIndex = 2;
            this.attendenceComponentID.Text = "Attendence Component ID";
            // 
            // rubricMeasurnmentID
            // 
            this.rubricMeasurnmentID.AutoSize = true;
            this.rubricMeasurnmentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricMeasurnmentID.Location = new System.Drawing.Point(66, 222);
            this.rubricMeasurnmentID.Name = "rubricMeasurnmentID";
            this.rubricMeasurnmentID.Size = new System.Drawing.Size(264, 29);
            this.rubricMeasurnmentID.TabIndex = 3;
            this.rubricMeasurnmentID.Text = "Rubric Measurnment ID";
            // 
            // evaluationDate
            // 
            this.evaluationDate.AutoSize = true;
            this.evaluationDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.evaluationDate.Location = new System.Drawing.Point(66, 283);
            this.evaluationDate.Name = "evaluationDate";
            this.evaluationDate.Size = new System.Drawing.Size(180, 29);
            this.evaluationDate.TabIndex = 4;
            this.evaluationDate.Text = "Evaluation Date";
            // 
            // studentIDbox
            // 
            this.studentIDbox.FormattingEnabled = true;
            this.studentIDbox.Location = new System.Drawing.Point(465, 83);
            this.studentIDbox.Name = "studentIDbox";
            this.studentIDbox.Size = new System.Drawing.Size(295, 24);
            this.studentIDbox.TabIndex = 5;
            this.studentIDbox.SelectedValueChanged += new System.EventHandler(this.studentIDbox_SelectedValueChanged);
            // 
            // attendenceCompIDBox
            // 
            this.attendenceCompIDBox.FormattingEnabled = true;
            this.attendenceCompIDBox.Location = new System.Drawing.Point(465, 153);
            this.attendenceCompIDBox.Name = "attendenceCompIDBox";
            this.attendenceCompIDBox.Size = new System.Drawing.Size(295, 24);
            this.attendenceCompIDBox.TabIndex = 6;
            this.attendenceCompIDBox.SelectedValueChanged += new System.EventHandler(this.attendenceCompIDBox_SelectedValueChanged);
            // 
            // rubricMeasurnmentIDBox
            // 
            this.rubricMeasurnmentIDBox.FormattingEnabled = true;
            this.rubricMeasurnmentIDBox.Location = new System.Drawing.Point(465, 222);
            this.rubricMeasurnmentIDBox.Name = "rubricMeasurnmentIDBox";
            this.rubricMeasurnmentIDBox.Size = new System.Drawing.Size(295, 24);
            this.rubricMeasurnmentIDBox.TabIndex = 7;
            this.rubricMeasurnmentIDBox.SelectedValueChanged += new System.EventHandler(this.rubricMeasurnmentIDBox_SelectedValueChanged);
            // 
            // addStudentResult
            // 
            this.addStudentResult.Location = new System.Drawing.Point(628, 351);
            this.addStudentResult.Name = "addStudentResult";
            this.addStudentResult.Size = new System.Drawing.Size(132, 43);
            this.addStudentResult.TabIndex = 9;
            this.addStudentResult.Text = "Add Student Result";
            this.addStudentResult.UseVisualStyleBackColor = true;
            this.addStudentResult.Click += new System.EventHandler(this.addStudentResult_Click);
            // 
            // evaluationDatebox
            // 
            this.evaluationDatebox.Location = new System.Drawing.Point(465, 283);
            this.evaluationDatebox.Name = "evaluationDatebox";
            this.evaluationDatebox.Size = new System.Drawing.Size(295, 22);
            this.evaluationDatebox.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(793, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(299, 29);
            this.label1.TabIndex = 11;
            this.label1.Text = "Rubric Measurnment Level";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1168, 215);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 29);
            this.label2.TabIndex = 12;
            this.label2.Text = "lable";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1168, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 29);
            this.label3.TabIndex = 14;
            this.label3.Text = "lable";
            // 
            // label4S
            // 
            this.label4S.AutoSize = true;
            this.label4S.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4S.Location = new System.Drawing.Point(793, 83);
            this.label4S.Name = "label4S";
            this.label4S.Size = new System.Drawing.Size(235, 29);
            this.label4S.TabIndex = 13;
            this.label4S.Text = "Registration Number";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1168, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 29);
            this.label5.TabIndex = 16;
            this.label5.Text = "lable";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(793, 148);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(345, 29);
            this.label6.TabIndex = 15;
            this.label6.Text = "Assessment Component Name";
            // 
            // studentResult
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 500);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4S);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.evaluationDatebox);
            this.Controls.Add(this.addStudentResult);
            this.Controls.Add(this.rubricMeasurnmentIDBox);
            this.Controls.Add(this.attendenceCompIDBox);
            this.Controls.Add(this.studentIDbox);
            this.Controls.Add(this.evaluationDate);
            this.Controls.Add(this.rubricMeasurnmentID);
            this.Controls.Add(this.attendenceComponentID);
            this.Controls.Add(this.studentID);
            this.Name = "studentResult";
            this.Text = "studentResult";
            this.Load += new System.EventHandler(this.studentResult_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label studentID;
        private System.Windows.Forms.Label attendenceComponentID;
        private System.Windows.Forms.Label rubricMeasurnmentID;
        private System.Windows.Forms.Label evaluationDate;
        private System.Windows.Forms.ComboBox studentIDbox;
        private System.Windows.Forms.ComboBox attendenceCompIDBox;
        private System.Windows.Forms.ComboBox rubricMeasurnmentIDBox;
        private System.Windows.Forms.Button addStudentResult;
        private System.Windows.Forms.DateTimePicker evaluationDatebox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4S;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}