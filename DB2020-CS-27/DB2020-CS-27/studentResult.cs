﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace DB2020_CS_27
{
    public partial class studentResult : Form
    {
        public studentResult()
        {
            InitializeComponent();
        }

        public void loadindAssessmentCompponentName()
        {
            cbi = (int)attendenceCompIDBox.SelectedItem;
            var con1 = Configuration.getInstance().getConnection();
            SqlDataReader myReader = null;
            SqlCommand cmd1 = new SqlCommand(" SELECT Name FROM AssessmentComponent   WHERE Id = @searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
            myReader = cmd1.ExecuteReader();
            while (myReader.Read())
            {
                //rubricMeasurnmentIDBox.Items.Add(myReader[0]);
                label5.Text = myReader["Name"].ToString();

            }
            myReader.Close();
            //loadindStudentID();
        }

        public void loadingStudentRegistrationNumber()
        {

            cbi = (int)studentIDbox.SelectedItem;
            var con1 = Configuration.getInstance().getConnection();
            SqlDataReader myReader = null;
            SqlCommand cmd1 = new SqlCommand(" SELECT RegistrationNumber FROM Student   WHERE Id = @searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
            myReader = cmd1.ExecuteReader();
            while (myReader.Read())
            {
                //rubricMeasurnmentIDBox.Items.Add(myReader[0]);
                label3.Text = myReader["RegistrationNumber"].ToString();

            }
            myReader.Close();
            //loadindStudentID();
        }

        private void attendenceDate_Click(object sender, EventArgs e)
        {

        }

        private void studentResult_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("  SELECT Id FROM Student   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();
            while (DR1.Read())
            {
                studentIDbox.Items.Add(DR1[0]);
            }
            DR1.Close();



            //SqlCommand cmd2 = new SqlCommand("  SELECT Id FROM AssessmentComponent   ", con);
            //SqlDataReader DR2 = cmd2.ExecuteReader();
            //while (DR2.Read())
            //{
            //    attendenceCompIDBox.Items.Add(DR2[0]);
            //}
            //DR2.Close();

            //SqlCommand cmd3 = new SqlCommand("  SELECT Id FROM RubricLevel   ", con);
            //SqlDataReader DR3 = cmd3.ExecuteReader();
            //while (DR3.Read())
            //{
            //    rubricMeasurnmentIDBox.Items.Add(DR3[0]);
            //}
            //DR3.Close();

        }

        private void addStudentResult_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" insert into StudentResult values(@studentIDbox , @attendenceCompIDBox   ,@rubricMeasurnmentIDBox   ,@evaluationDateBox  ) ", con);
            cmd.Parameters.AddWithValue("@studentIDbox", int.Parse(studentIDbox.Text));
            cmd.Parameters.AddWithValue("@attendenceCompIDBox", int.Parse(attendenceCompIDBox.Text));
            cmd.Parameters.AddWithValue("@rubricMeasurnmentIDBox", int.Parse(rubricMeasurnmentIDBox.Text));
            cmd.Parameters.AddWithValue("@evaluationDateBox", DateTime.Parse(evaluationDatebox.Text));



            cmd.ExecuteNonQuery();
            MessageBox.Show("Attendence Added Successfully ");
        }
        int cbi;
        private void studentIDbox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (studentIDbox.SelectedItem != null)
            {







                attendenceCompIDBox.Items.Clear();
                cbi = (int)studentIDbox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Id FROM AssessmentComponent EXCEPT( SELECT AssessmentComponentId FROM StudentResult WHERE StudentId = @searchID ) ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    attendenceCompIDBox.Items.Add(myReader[0]);
                    //attendenceCompIDBox.Text = myReader["Id"].ToString();

                }
                myReader.Close();
                loadingStudentRegistrationNumber();

            }
        }

        private void attendenceCompIDBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (attendenceCompIDBox.SelectedItem != null)
            {
                rubricMeasurnmentIDBox.Items.Clear();

                cbi = (int)attendenceCompIDBox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("  SELECT R.Id FROM ( SELECT RubricLevel.Id  from AssessmentComponent JOIN Rubric ON AssessmentComponent.RubricId = Rubric.Id JOIN RubricLevel ON Rubric.Id = RubricLevel.RubricId WHERE AssessmentComponent.Id = @searchID ) AS R  ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString() ));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    rubricMeasurnmentIDBox.Items.Add(myReader[0]);
                    //attendenceCompIDBox.Text = myReader["Id"].ToString();

                }
                myReader.Close();
                loadindAssessmentCompponentName();

            }
        }
        private void rubricMeasurnmentIDBox_SelectedValueChanged(object sender, EventArgs e)
        {
             cbi = (int)rubricMeasurnmentIDBox.SelectedItem;
            var con1 = Configuration.getInstance().getConnection();
            SqlDataReader myReader = null;
            SqlCommand cmd1 = new SqlCommand("SELECT MeasurementLevel FROM RubricLevel WHERE Id = @searchID  ", con1);
            cmd1.Parameters.AddWithValue("@searchID", (cbi.ToString()));
            myReader = cmd1.ExecuteReader();



            while (myReader.Read())
            {

                label2.Text = myReader["MeasurementLevel"].ToString();


            }
            myReader.Close();
        }
    }
}
