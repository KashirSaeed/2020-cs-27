﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{
    public partial class delete_And_Update_Assessment : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
  
        public delete_And_Update_Assessment()
        {
            InitializeComponent();
        }
        string nums;
        private void getRecord_Click(object sender, EventArgs e)
        {
            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM Assessment WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");

        }

        private void updateStudent_Click(object sender, EventArgs e)
        {
            if (regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString()) && regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString()) && regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[4].FormattedValue.ToString()))          
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(" UPDATE Assessment SET Title = @tittle  ,DateCreated = @dateCreated , TotalMarks = @totalMarks , TotalWeightage = @totalWeitage    where  Id = @searchID  ", con);
                cmd.Parameters.AddWithValue("@tittle", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@dateCreated", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@totalMarks", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@totalWeitage", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[4].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));


                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
                
            }
            else
            {
                MessageBox.Show("please update in correct format");
            }


               
        }

        private void deleteStudent_Click(object sender, EventArgs e)
        {
            List<int> tempList = new List<int>();


            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd3 = new SqlCommand("SELECT Id  FROM AssessmentComponent where AssessmentId = @searchID", con);
            cmd3.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            SqlDataReader Sdr = cmd3.ExecuteReader();
            int idx=0;
            while (Sdr.Read())
            {
                tempList.Add(int.Parse(Sdr.GetValue(0).ToString())); 
            }

            Sdr.Close();
            for(int i = 0; i < tempList.Count; i++) {
                SqlCommand cmd4 = new SqlCommand(" DELETE from StudentResult where  AssessmentComponentId = @searchid ", con);
                cmd4.Parameters.AddWithValue("@searchid",  int.Parse(tempList[i].ToString()) );
                cmd4.ExecuteNonQuery();
            }

            SqlCommand cmd1 = new SqlCommand(" DELETE from AssessmentComponent where  AssessmentId = @searchID ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd1.ExecuteNonQuery();

            SqlCommand cmd2 = new SqlCommand(" DELETE from Assessment where  Id = @searchID ", con);
            cmd2.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd2.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
            con.Close();
        }

        private void delete_And_Update_Assessment_Load(object sender, EventArgs e)
        {

        }

        private void delete_And_Update_Assessment_Load_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT  Id FROM Assessment   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();
        }

        private void update_And_Delete_Table_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Any of the previuosly cell is edited incorrectly");
            e.Cancel = true;
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT Title FROM Assessment WHERE Id =@searchID ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            SqlDataReader sdr = cmd1.ExecuteReader();
            sdr.Read();
            value.Text = sdr["Title"].ToString();
            sdr.Close();
        }
    }
}
