﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{
    public partial class Rubric : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex3 = new Regex("^[0-9]*$");
       
        public Rubric()
        {
            InitializeComponent();
            warningLable.Hide();

        }
        int cbi;
        private void cloidComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cloidComboBox.SelectedItem != null)
            {

                cbi = (int)cloidComboBox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Name  FROM Clo WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label2.Text = myReader["Name"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }

        private void cloidComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        string nums;
        private void addRubric_Click(object sender, EventArgs e)
        {
            if (regex3.IsMatch(rubricIDTextbox.Text) && regex1.IsMatch(rubDetail.Text))
            {

                if (rubricIDTextbox.Text == "" || rubDetail.Text == "" || cloidComboBox.Text == "")
                {
                    warningLable.Show();
                }
                else
                {
                    warningLable.Hide();
                    nums = new String(cloidComboBox.Text.Where(Char.IsDigit).ToArray());
                    var con1 = Configuration.getInstance().getConnection();
                    SqlCommand cmd1 = new SqlCommand("insert into Rubric values(@rubricIDTextbox,@rubDetail , @searchID) ", con1);
                    cmd1.Parameters.AddWithValue("@rubricIDTextbox", rubricIDTextbox.Text);
                    cmd1.Parameters.AddWithValue("@rubDetail", rubDetail.Text);
                    cmd1.Parameters.AddWithValue("@searchID", int.Parse(nums));

                    cmd1.ExecuteNonQuery();
                    MessageBox.Show("rubric Added Successfully ");
                }
            }
            else
            {
                MessageBox.Show(warningLable.Text);
            }

            

            //var con = Configuration.getInstance().getConnection();
            //SqlCommand cmd = new SqlCommand(" insert into Rubric values(@rubricIDTextbox,@rubDetail , @cloidComboBox ) ", con);
            //cmd.Parameters.AddWithValue("@rubricIDTextbox", rubricIDTextbox.Text);
            //cmd.Parameters.AddWithValue("@rubDetail", rubDetail.Text);
            //cmd.Parameters.AddWithValue("@cloidComboBox", int.Parse(cloidComboBox.Text));

            //cmd.ExecuteNonQuery();
            //MessageBox.Show("rubric Added Successfully ");
        }

        private void Rubric_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT  Id   FROM Clo   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                cloidComboBox.Items.Add(DR1[0]);
            }
            DR1.Close();
        }
    }
}
