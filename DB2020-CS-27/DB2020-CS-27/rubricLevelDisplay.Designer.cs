﻿
namespace DB2020_CS_27
{
    partial class rubricLevelDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.disRubricLevel = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.disRubricLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // disRubricLevel
            // 
            this.disRubricLevel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.disRubricLevel.Location = new System.Drawing.Point(48, 91);
            this.disRubricLevel.Name = "disRubricLevel";
            this.disRubricLevel.RowHeadersWidth = 51;
            this.disRubricLevel.RowTemplate.Height = 24;
            this.disRubricLevel.Size = new System.Drawing.Size(704, 269);
            this.disRubricLevel.TabIndex = 2;
            // 
            // rubricLevelDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.disRubricLevel);
            this.Name = "rubricLevelDisplay";
            this.Text = "rubricLevelDisplay";
            this.Load += new System.EventHandler(this.rubricLevelDisplay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.disRubricLevel)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView disRubricLevel;
    }
}