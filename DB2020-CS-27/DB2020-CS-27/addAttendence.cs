﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace DB2020_CS_27
{
    public partial class addAttendence : Form
    {
        public addAttendence()
        {
            InitializeComponent();
        }

        private void addDate_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" insert into ClassAttendance values(@attendenceDate  ) ", con);
            cmd.Parameters.AddWithValue("@attendenceDate", DateTime.Parse(dateTimePicker1.Text));


            cmd.ExecuteNonQuery();
            MessageBox.Show("Attendence Added Successfully ");
        }

        private void addAttendence_Load(object sender, EventArgs e)
        {

        }
    }
}
