﻿
namespace DB2020_CS_27
{
    partial class attendenceDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayAttendence = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.displayAttendence)).BeginInit();
            this.SuspendLayout();
            // 
            // displayAttendence
            // 
            this.displayAttendence.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displayAttendence.Location = new System.Drawing.Point(176, 118);
            this.displayAttendence.Name = "displayAttendence";
            this.displayAttendence.RowHeadersWidth = 51;
            this.displayAttendence.RowTemplate.Height = 24;
            this.displayAttendence.Size = new System.Drawing.Size(428, 150);
            this.displayAttendence.TabIndex = 0;
            // 
            // attendenceDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.displayAttendence);
            this.Name = "attendenceDisplay";
            this.Text = "displayAttendenceDate";
            this.Load += new System.EventHandler(this.displayAttendenceDate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.displayAttendence)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView displayAttendence;
    }
}