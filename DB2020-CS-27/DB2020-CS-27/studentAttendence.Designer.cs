﻿
namespace DB2020_CS_27
{
    partial class studentAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.attendenceDate = new System.Windows.Forms.Label();
            this.studentRegistration = new System.Windows.Forms.Label();
            this.attendenceIDCombobox = new System.Windows.Forms.ComboBox();
            this.studentIDCombobox = new System.Windows.Forms.ComboBox();
            this.attendenceStatusCombo = new System.Windows.Forms.ComboBox();
            this.idValue = new System.Windows.Forms.Label();
            this.attendence = new System.Windows.Forms.Button();
            this.markAttendence = new System.Windows.Forms.Label();
            this.statusOfAttendence = new System.Windows.Forms.Label();
            this.stuID = new System.Windows.Forms.Label();
            this.category = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // attendenceDate
            // 
            this.attendenceDate.AutoSize = true;
            this.attendenceDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendenceDate.Location = new System.Drawing.Point(55, 24);
            this.attendenceDate.Name = "attendenceDate";
            this.attendenceDate.Size = new System.Drawing.Size(163, 29);
            this.attendenceDate.TabIndex = 0;
            this.attendenceDate.Text = "Attendence ID";
            // 
            // studentRegistration
            // 
            this.studentRegistration.AutoSize = true;
            this.studentRegistration.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentRegistration.Location = new System.Drawing.Point(55, 105);
            this.studentRegistration.Name = "studentRegistration";
            this.studentRegistration.Size = new System.Drawing.Size(124, 29);
            this.studentRegistration.TabIndex = 1;
            this.studentRegistration.Text = "Student ID";
            // 
            // attendenceIDCombobox
            // 
            this.attendenceIDCombobox.FormattingEnabled = true;
            this.attendenceIDCombobox.Location = new System.Drawing.Point(390, 31);
            this.attendenceIDCombobox.Name = "attendenceIDCombobox";
            this.attendenceIDCombobox.Size = new System.Drawing.Size(282, 24);
            this.attendenceIDCombobox.TabIndex = 3;
            this.attendenceIDCombobox.SelectedIndexChanged += new System.EventHandler(this.attendenceIDCombobox_SelectedIndexChanged);
            this.attendenceIDCombobox.SelectedValueChanged += new System.EventHandler(this.attendenceIDCombobox_SelectedValueChanged);
            // 
            // studentIDCombobox
            // 
            this.studentIDCombobox.FormattingEnabled = true;
            this.studentIDCombobox.Location = new System.Drawing.Point(390, 110);
            this.studentIDCombobox.Name = "studentIDCombobox";
            this.studentIDCombobox.Size = new System.Drawing.Size(282, 24);
            this.studentIDCombobox.TabIndex = 4;
            this.studentIDCombobox.SelectedIndexChanged += new System.EventHandler(this.studentIDCombobox_SelectedIndexChanged);
            // 
            // attendenceStatusCombo
            // 
            this.attendenceStatusCombo.FormattingEnabled = true;
            this.attendenceStatusCombo.Items.AddRange(new object[] {
            "Present",
            "Absent",
            "Leave",
            "Late"});
            this.attendenceStatusCombo.Location = new System.Drawing.Point(390, 196);
            this.attendenceStatusCombo.Name = "attendenceStatusCombo";
            this.attendenceStatusCombo.Size = new System.Drawing.Size(282, 24);
            this.attendenceStatusCombo.TabIndex = 5;
            this.attendenceStatusCombo.SelectedValueChanged += new System.EventHandler(this.attendenceStatusCombo_SelectedValueChanged);
            // 
            // idValue
            // 
            this.idValue.AutoSize = true;
            this.idValue.Location = new System.Drawing.Point(1070, 38);
            this.idValue.Name = "idValue";
            this.idValue.Size = new System.Drawing.Size(0, 17);
            this.idValue.TabIndex = 7;
            this.idValue.Click += new System.EventHandler(this.idValue_Click);
            // 
            // attendence
            // 
            this.attendence.Location = new System.Drawing.Point(554, 273);
            this.attendence.Name = "attendence";
            this.attendence.Size = new System.Drawing.Size(118, 49);
            this.attendence.TabIndex = 8;
            this.attendence.Text = "Done";
            this.attendence.UseVisualStyleBackColor = true;
            this.attendence.Click += new System.EventHandler(this.attendence_Click);
            // 
            // markAttendence
            // 
            this.markAttendence.AutoSize = true;
            this.markAttendence.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.markAttendence.Location = new System.Drawing.Point(55, 191);
            this.markAttendence.Name = "markAttendence";
            this.markAttendence.Size = new System.Drawing.Size(193, 29);
            this.markAttendence.TabIndex = 9;
            this.markAttendence.Text = "Mark Attendence";
            // 
            // statusOfAttendence
            // 
            this.statusOfAttendence.AutoSize = true;
            this.statusOfAttendence.Location = new System.Drawing.Point(1070, 203);
            this.statusOfAttendence.Name = "statusOfAttendence";
            this.statusOfAttendence.Size = new System.Drawing.Size(0, 17);
            this.statusOfAttendence.TabIndex = 10;
            // 
            // stuID
            // 
            this.stuID.AutoSize = true;
            this.stuID.Location = new System.Drawing.Point(1070, 116);
            this.stuID.Name = "stuID";
            this.stuID.Size = new System.Drawing.Size(0, 17);
            this.stuID.TabIndex = 11;
            // 
            // category
            // 
            this.category.AutoSize = true;
            this.category.Location = new System.Drawing.Point(1049, 304);
            this.category.Name = "category";
            this.category.Size = new System.Drawing.Size(0, 17);
            this.category.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(728, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 29);
            this.label1.TabIndex = 13;
            this.label1.Text = "Attendance Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(728, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(235, 29);
            this.label2.TabIndex = 14;
            this.label2.Text = "Registration Number";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(728, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 29);
            this.label3.TabIndex = 15;
            this.label3.Text = "Attendance Status";
            // 
            // studentAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 378);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.category);
            this.Controls.Add(this.stuID);
            this.Controls.Add(this.statusOfAttendence);
            this.Controls.Add(this.markAttendence);
            this.Controls.Add(this.attendence);
            this.Controls.Add(this.idValue);
            this.Controls.Add(this.attendenceStatusCombo);
            this.Controls.Add(this.studentIDCombobox);
            this.Controls.Add(this.attendenceIDCombobox);
            this.Controls.Add(this.studentRegistration);
            this.Controls.Add(this.attendenceDate);
            this.Name = "studentAttendence";
            this.Text = "studentAttendence";
            this.Load += new System.EventHandler(this.studentAttendence_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label attendenceDate;
        private System.Windows.Forms.Label studentRegistration;
        private System.Windows.Forms.ComboBox attendenceIDCombobox;
        private System.Windows.Forms.ComboBox studentIDCombobox;
        private System.Windows.Forms.ComboBox attendenceStatusCombo;
        private System.Windows.Forms.Label idValue;
        private System.Windows.Forms.Button attendence;
        private System.Windows.Forms.Label markAttendence;
        private System.Windows.Forms.Label statusOfAttendence;
        private System.Windows.Forms.Label stuID;
        private System.Windows.Forms.Label category;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}