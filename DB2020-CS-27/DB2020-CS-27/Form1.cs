﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DB2020_CS_27
{
    public partial class Form1 : Form
    {
        Form currentForm;
        public void displayForm( Form childForm)
        {
            if (currentForm != null)
            {
                currentForm.Close();
            }
            currentForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            mainPanel.Controls.Add(childForm);
            mainPanel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }


        private void HideAllSubMenu()
        {
            subPanel1.Visible = false;
            subPanel2.Visible = false;
            subPanel3.Visible = false;
            subPanel4.Visible = false;
            subPanel5.Visible = false;
        }

        public void showsubmenu(Panel submenu)
        {

            if (submenu.Visible == false)
            {
                HideAllSubMenu();
                submenu.Visible = true;
            }
            else
            {
                submenu.Visible = false;
            }
        }
        public Form1()
        {
            InitializeComponent();
            HideAllSubMenu();
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tittle_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            showsubmenu(subPanel1);
        }

   

        //private void button9_Click(object sender, EventArgs e)
        //{
        //    showsubmenu(subPanel3);
        //}

        private void addStudent_Click(object sender, EventArgs e)
        {
            displayForm(new Add_Student());
        }

        private void displayStudent_Click(object sender, EventArgs e)
        {
            displayForm(new display_Student());
        }

        private void delete_And_Update_Student_Click(object sender, EventArgs e)
        {
            displayForm(new delete_And_Update());
        }

        private void addAssessment_Click(object sender, EventArgs e)
        {
            displayForm(new addAssessment());
        }

        private void assessment_Click(object sender, EventArgs e)
        {
            showsubmenu(subPanel2);
        }

        private void dispalyAssessment_Click(object sender, EventArgs e)
        {
            displayForm(new displayAssessment());
        }

        private void delete_And_Update_Assessment_Click(object sender, EventArgs e)
        {
            displayForm(new delete_And_Update_Assessment());
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void addCLO_Click(object sender, EventArgs e)
        {
            
        }

        private void CLO_Click(object sender, EventArgs e)
        {
            showsubmenu(subPanel3);
        }

        private void addCLO_Click_1(object sender, EventArgs e)
        {
            displayForm(new addCLO());
        }

        private void displayCLO_Click(object sender, EventArgs e)
        {
            displayForm(new displayCLO());
        }

        private void deleteAndUpdateCLO_Click(object sender, EventArgs e)
        {
            displayForm(new delete_and_Update_CLO());
        }

        private void addAttendenceDate_Click(object sender, EventArgs e)
        {
            displayForm(new addAttendence());
        }

        private void classAttendence_Click(object sender, EventArgs e)
        {
            showsubmenu(subPanel4);
        }

        private void displayAttendenceDate_Click(object sender, EventArgs e)
        {
            displayForm(new attendenceDisplay());
        }

        private void deleteAndUpdateAttendenceDate_Click(object sender, EventArgs e)
        {
            displayForm(new deleteAndUpdateClassAttendence());
        }

        private void studentAttendence_Click(object sender, EventArgs e)
        {
            displayForm(new studentAttendence());
;        }

        private void rubric_Click(object sender, EventArgs e)
        {
            displayForm(new  Rubric());
        }

        private void rubricLevel_Click(object sender, EventArgs e)
        {
            displayForm(new RubricLevel());

        }

        private void addAssessmentComponent_Click(object sender, EventArgs e)
        {
            displayForm(new addAssessmentComponent());

        }

        private void subPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void assessmentComponent_Click(object sender, EventArgs e)
        {
            showsubmenu(subPanel5);

        }

        private void displayAssessmentComponent_Click(object sender, EventArgs e)
        {
            displayForm(new displayAssessmentComponent());
        }

        private void updateAssessmentComponent_Click(object sender, EventArgs e)
        {
            displayForm(new updateAssessmentComponent());

        }

        private void studentResult_Click(object sender, EventArgs e)
        {

        }

        private void studentResult_Click_1(object sender, EventArgs e)
        {
            displayForm(new studentResult());

        }

        private void displayStudentAttendence_Click(object sender, EventArgs e)
        {
            displayForm(new displayStudentAttendence());
        }

        private void displayRubrics_Click(object sender, EventArgs e)
        {
            displayForm(new rubricDisplay());
        }

        private void displayRubricLevel_Click(object sender, EventArgs e)
        {
            displayForm(new rubricLevelDisplay());
        }

        private void displayStudentResult_Click(object sender, EventArgs e)
        {
            displayForm(new studentResultDisplay());
        }

        private void deleteAndUpdateRubric_Click(object sender, EventArgs e)
        {
            displayForm(new deleteAndUpdateRubric());
        }

        private void deleteAndUpdateRubricLevel_Click(object sender, EventArgs e)
        {
            displayForm(new deleteAndUpdateRubricLevel());
        }

        private void deleteAndUpdateStudentResult_Click(object sender, EventArgs e)
        {
            displayForm(new deleteAndUpdateStudentResult());
        }

        private void deleteAndUpdateStudentAttendence_Click(object sender, EventArgs e)
        {
            displayForm(new deleteAndUpdateStudentAttendence());
        }

        private void PDF_Generator_Buttton_Click(object sender, EventArgs e)
        {
            displayForm(new PDF_Form());
        }
    }
}
