﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{
  
    public partial class updateAssessmentComponent : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        string nums;
        public updateAssessmentComponent()
        {
            InitializeComponent();
        }

        private void updateAssessmentComponent_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT  Id FROM AssessmentComponent   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                searchIDCombo.Items.Add(DR1[0]);

            }
            DR1.Close();
        }
        int marks_At_GetingRecordTime = 0;
        private void getRecord_Click(object sender, EventArgs e)
        {
            

            nums = new String(searchIDCombo.Text.Where(Char.IsDigit).ToArray());

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM AssessmentComponent WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID",int.Parse(nums) );

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            //update_And_Delete_Table.Columns["TotalMarks"].ReadOnly = true;
            update_And_Delete_Table.Columns["DateCreated"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");
            marks_At_GetingRecordTime = int.Parse(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
            MessageBox.Show(marks_At_GetingRecordTime.ToString());
        }
        string tempCloid1;
        string tempCloid2;
        int flag1 = 0;
        int flag2 = 0;

        //string nums;
        //string nums1;
        int totalAssessmentMarks;
        int marks = 0;
        int tempMarks = 0;
        int sum = 0;
        string temp;

        private void updateAssessment_Click(object sender, EventArgs e)
        {
            marks = 0;
            sum = 0;
            tempMarks = 0;
            temp = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[6].FormattedValue.ToString();

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd5 = new SqlCommand("    SELECT TotalMarks  FROM Assessment   WHERE Id = @searchID      ", con1);
            cmd5.Parameters.AddWithValue("@searchID", temp);
            SqlDataReader myReader = cmd5.ExecuteReader();
            while (myReader.Read())
            {
                //comboBox2.Items.Add(myReader[0]);
                totalAssessmentMarks = int.Parse(myReader["TotalMarks"].ToString());
            }
            myReader.Close();
            //MessageBox.Show(totalAssessmentMarks);
            cmd5.ExecuteNonQuery();
            MessageBox.Show("AssessmentComponenet Added Successfully ");






            SqlCommand cmd6 = new SqlCommand("    SELECT TotalMarks  FROM AssessmentComponent  WHERE AssessmentId =   @searchID   ", con1);
            cmd6.Parameters.AddWithValue("@searchID", temp);
            SqlDataReader myReader1 = cmd6.ExecuteReader();
            while (myReader1.Read())
            {
                //comboBox2.Items.Add(myReader[0]);
                sum = int.Parse(myReader1["TotalMarks"].ToString());
                marks = marks + sum;
            }
            tempMarks = marks;
            marks = marks - marks_At_GetingRecordTime;
            marks = marks + int.Parse(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
            myReader1.Close();
            //MessageBox.Show(marks.ToString());
            cmd6.ExecuteNonQuery();
            MessageBox.Show("AssessmentComponenet Added Successfully ");

            if (marks <= totalAssessmentMarks)
            {
                flag1 = 0;
                flag2 = 0;
                tempCloid1 = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString();
                tempCloid2 = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[6].FormattedValue.ToString();

                //var con1 = Configuration.getInstance().getConnection();
                SqlCommand cmd2 = new SqlCommand("  SELECT Id FROM Assessment where Id = @searchID2  ", con1);
                cmd2.Parameters.AddWithValue("@searchID2", int.Parse(tempCloid2));
                SqlDataReader DR2 = cmd2.ExecuteReader();

                while (DR2.Read())
                {
                    flag2 = 1;
                }
                DR2.Close();

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("  SELECT Id FROM Rubric where Id = @searchID1  ", con);
                cmd1.Parameters.AddWithValue("@searchID1", int.Parse(tempCloid1));
                SqlDataReader DR1 = cmd1.ExecuteReader();
                while (DR1.Read())
                {

                    flag1 = 1;
                }
                DR1.Close();

                if (flag1 == 0 && flag2 == 0)
                {
                    MessageBox.Show("this rubric id and assessment id is not added. please enter the correct one!!!!!!");
                }
                if (flag1 == 0 && flag2 == 1)
                {
                    MessageBox.Show("this rubric id is not added. please enter the correct one!!!!!!");

                }
                if (flag1 == 1 && flag2 == 0)
                {
                    MessageBox.Show("this  assessment id is not added. please enter the correct one!!!!!!");

                }
                if (flag1 == 1 && flag2 == 1)
                {
                    MessageBox.Show("done");
                    if (regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString()) && regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString()))
                    {

                        //var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(" UPDATE AssessmentComponent SET Name = @studentID  ,RubricId = @studentName , TotalMarks = @courseName   , DateUpdated = @updatedDate , AssessmentId = @grade   where Id =@searchID   ", con);
                        cmd.Parameters.AddWithValue("@studentID", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                        cmd.Parameters.AddWithValue("@studentName", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                        cmd.Parameters.AddWithValue("@courseName", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                        //cmd.Parameters.AddWithValue("@marks", DateTime.Now.Date.ToString());
                        cmd.Parameters.AddWithValue("@updatedDate", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[5].FormattedValue.ToString());

                        cmd.Parameters.AddWithValue("@grade", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[6].FormattedValue.ToString());
                        cmd.Parameters.AddWithValue("@searchID", int.Parse(nums));


                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully updated");
                    }
                    else
                    {
                        MessageBox.Show(" please update in correct format ");
                    }
                }
                






            }
            else
            {
                MessageBox.Show("you can add the the total marks of assessment component upto:    " + (totalAssessmentMarks - tempMarks).ToString());
            }





























        }

        private void searchID_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void update_And_Delete_Table_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Any of the previuosly cell is edited incorrectly");
            e.Cancel = true;
        }

        private void deleteAssessmentComponent_Click(object sender, EventArgs e)
        {
            nums = new String(searchIDCombo.Text.Where(Char.IsDigit).ToArray());

            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd4 = new SqlCommand(" DELETE from StudentResult where  AssessmentComponentId = @searchID  ", con);
            cmd4.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd4.ExecuteNonQuery();

            SqlCommand cmd3 = new SqlCommand(" DELETE from AssessmentComponent where  Id = @searchID  ", con);
            cmd3.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd3.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        
        }
        int cbi;
        private void searchID_SelectedValueChanged(object sender, EventArgs e)
        {
            if (searchIDCombo.SelectedItem != null)
            {

                cbi = (int)searchIDCombo.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Name  FROM AssessmentComponent WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label2.Text = myReader["Name"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }
    }
}
