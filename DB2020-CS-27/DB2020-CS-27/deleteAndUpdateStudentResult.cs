﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace DB2020_CS_27
{
    public partial class deleteAndUpdateStudentResult : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        public deleteAndUpdateStudentResult()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        public void loadindAssessmentCompponentName()
        {
            cbi = (int)comboBox2.SelectedItem;
            var con1 = Configuration.getInstance().getConnection();
            SqlDataReader myReader = null;
            SqlCommand cmd1 = new SqlCommand(" SELECT Name FROM AssessmentComponent   WHERE Id = @searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
            myReader = cmd1.ExecuteReader();
            while (myReader.Read())
            {
                //rubricMeasurnmentIDBox.Items.Add(myReader[0]);
                label3.Text = myReader["Name"].ToString();

            }
            myReader.Close();
            //loadindStudentID();
        }
        public void loadingStudentRegistrationNumber()
        {

            cbi = (int)comboBox1.SelectedItem;
            var con1 = Configuration.getInstance().getConnection();
            SqlDataReader myReader = null;
            SqlCommand cmd1 = new SqlCommand(" SELECT RegistrationNumber FROM Student   WHERE Id = @searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
            myReader = cmd1.ExecuteReader();
            while (myReader.Read())
            {
                //rubricMeasurnmentIDBox.Items.Add(myReader[0]);
                label4.Text = myReader["RegistrationNumber"].ToString();

            }
            myReader.Close();
            //loadindStudentID();
        }
        private void deleteAndUpdateStudentResult_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT DISTINCT StudentId  FROM StudentResult   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();


            //SqlCommand cmd1 = new SqlCommand("  SELECT Id  FROM AssessmentComponent   ", con);
            //SqlDataReader DR2 = cmd1.ExecuteReader();

            //while (DR2.Read())
            //{
            //    comboBox2.Items.Add(DR2[0]);

            //}
            //DR2.Close();
        }
        string num1;
        string num2;
        private void getRecord_Click(object sender, EventArgs e)
        {
            num1 = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());
            num2 = new String(comboBox2.Text.Where(Char.IsDigit).ToArray());


            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM StudentResult WHERE StudentId =@searchID1 and AssessmentComponentId = @searchID2 ", con1);
            cmd1.Parameters.AddWithValue("@searchID1", int.Parse(num1));
            cmd1.Parameters.AddWithValue("@searchID2", int.Parse(num2));


            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["StudentId"].ReadOnly = true;
            update_And_Delete_Table.Columns["AssessmentComponentId"].ReadOnly = true;

            MessageBox.Show("Successfully displayed");
        }
        string tempCloid;
        int flag = 0;
        private void updateAssessment_Click(object sender, EventArgs e)
        {
            tempCloid = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString();


            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("  SELECT Id FROM RubricLevel   ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(tempCloid));
            SqlDataReader DR1 = cmd1.ExecuteReader();

            while (DR1.Read())
            {
                flag = 0;
                //MessageBox.Show("kashir saeed");
                if (DR1.GetValue(0).ToString() == tempCloid)
                {
                    warningLable.Hide();
                    if (regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString()))
                    {
                        flag = 1;
                        DR1.Close();

                        //var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(" UPDATE StudentResult SET RubricMeasurementId = @rubric, EvaluationDate = @evaluation  where  StudentId = @searchID1 AND AssessmentComponentId = @searchID2  ", con);
                        cmd.Parameters.AddWithValue("@rubric", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                        cmd.Parameters.AddWithValue("@evaluation", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());

                        cmd.Parameters.AddWithValue("@searchID1", int.Parse(num1));
                        cmd.Parameters.AddWithValue("@searchID2", int.Parse(num2));



                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Successfully updated");
                        break;

                    }
                    else
                    {
                        MessageBox.Show("please update in correct format");
                    }
                }
                else
                {
                    warningLable.Show();
                }
            }
            if (flag != 1)
            {
                DR1.Close();

            }






        }

        private void deleteAssessment_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd3 = new SqlCommand(" DELETE from StudentResult where  StudentId = @searchID1 AND AssessmentComponentId = @searchID2  ", con);
            cmd3.Parameters.AddWithValue("@searchID1", int.Parse(num1));
            cmd3.Parameters.AddWithValue("@searchID2", int.Parse(num2));

            cmd3.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }
        int cbi;
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                comboBox2.Items.Clear();

                cbi = (int)comboBox1.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("  SELECT AssessmentComponentId  FROM StudentResult   WHERE StudentId =  @searchID", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    comboBox2.Items.Add(myReader[0]);
                    //attendenceCompIDBox.Text = myReader["Id"].ToString();

                }
                myReader.Close();
                loadingStudentRegistrationNumber();

            }
        }

        private void comboBox2_SelectedValueChanged(object sender, EventArgs e)
        {
            loadindAssessmentCompponentName();
        }
    }
}
