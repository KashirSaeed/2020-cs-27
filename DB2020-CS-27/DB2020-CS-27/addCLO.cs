﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;



namespace DB2020_CS_27
{
    public partial class addCLO : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");

        public addCLO()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        private void addCLOButton_Click(object sender, EventArgs e)
        {
            if (regex1.IsMatch(textBox1.Text))
            {

                if (textBox1.Text == "")
                {
                    warningLable.Show();
                }
                else
                {
                    warningLable.Hide();
                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd = new SqlCommand(" insert into Clo values(@cloName , @dateCreated , @dateUpdated   ) ", con);
                    cmd.Parameters.AddWithValue("@cloName", textBox1.Text);
                    cmd.Parameters.AddWithValue("@dateCreated", DateTime.Parse(dateTimePicker1.Text));
                    cmd.Parameters.AddWithValue("@dateUpdated", DateTime.Parse(dateTimePicker2.Text));


                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Student Added Successfully ");
                }
            }
            else
            {
                MessageBox.Show(warningLable.Text);
            }
            
        }

        private void addCLO_Load(object sender, EventArgs e)
        {

        }
    }
}
