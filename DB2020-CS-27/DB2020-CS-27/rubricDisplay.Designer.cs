﻿
namespace DB2020_CS_27
{
    partial class rubricDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.disRubric = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.disRubric)).BeginInit();
            this.SuspendLayout();
            // 
            // disRubric
            // 
            this.disRubric.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.disRubric.Location = new System.Drawing.Point(48, 91);
            this.disRubric.Name = "disRubric";
            this.disRubric.RowHeadersWidth = 51;
            this.disRubric.RowTemplate.Height = 24;
            this.disRubric.Size = new System.Drawing.Size(704, 269);
            this.disRubric.TabIndex = 2;
            // 
            // rubricDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.disRubric);
            this.Name = "rubricDisplay";
            this.Text = "rubricDisplay";
            this.Load += new System.EventHandler(this.rubricDisplay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.disRubric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView disRubric;
    }
}