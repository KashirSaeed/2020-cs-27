﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{
    public partial class Add_Student : Form
    {
        ErrorProvider errorProvider1 = new ErrorProvider();
        public Add_Student()
        {
            InitializeComponent();
            warningLable.Hide();
        }
        private void addStudentButton_Click(object sender, EventArgs e)
        {
           

            Regex regex1 = new Regex("^[a-z A-z]*$");
            Regex regex2 = new Regex("^[a-z A-z0-9]*$");
            Regex regex3 = new Regex("^[0-9]*$");
            Regex regex4 = new Regex("[a-z0-9!#$%&'+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'+/=?^_`{|}~-]+)@(?:[a-z0-9](?:[a-z0-9-][a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");



            if (regex1.IsMatch(textBox1.Text)  && regex1.IsMatch(textBox2.Text) && regex3.IsMatch(textBox3.Text) && regex2.IsMatch(textBox5.Text) )
            {



                warningLable.Hide();
                
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "" )
                {
                    warningLable.Show();
                }
                else
                {
                    if (studentStatus.Checked)
                    {

                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(" insert into Student values(@firstName , @lastName , @contact , @email , @registrationNumber , @status   ) ", con);
                        cmd.Parameters.AddWithValue("@firstName", textBox1.Text);
                        cmd.Parameters.AddWithValue("@lastName", textBox2.Text);
                        cmd.Parameters.AddWithValue("@contact", textBox3.Text);
                        cmd.Parameters.AddWithValue("@email", (textBox4.Text));
                        cmd.Parameters.AddWithValue("@registrationNumber", textBox5.Text);
                        cmd.Parameters.AddWithValue("@status", int.Parse("5"));

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Student Added Successfully ");
                    }

                    else
                    {

                        warningLable.Hide();
                        var con = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(" insert into Student values(@firstName , @lastName , @contact , @email , @registrationNumber , @status   ) ", con);
                        cmd.Parameters.AddWithValue("@firstName", textBox1.Text);
                        cmd.Parameters.AddWithValue("@lastName", textBox2.Text);
                        cmd.Parameters.AddWithValue("@contact", textBox3.Text);
                        cmd.Parameters.AddWithValue("@email", (textBox4.Text));
                        cmd.Parameters.AddWithValue("@registrationNumber", textBox5.Text);
                        cmd.Parameters.AddWithValue("@status", int.Parse("6"));

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Student Added Successfully ");
                    }
                    //}
                }

            }
            else
            {
                MessageBox.Show(warningLable.Text);
                //if (regex1.IsMatch(textBox1.Text) == false)
                //{
                //    textBox1.Focus();
                //    errorProvider1.SetError(textBox1, "Only numbers may be entered here");

                //}
                //else
                //{
                //    errorProvider1.Clear();
                //}

                //if (regex1.IsMatch(textBox2.Text) == false)
                //{
                //    textBox2.Focus();
                //    errorProvider1.SetError(textBox2, "Only numbers may be entered here");

                //}
                //else
                //{
                //    errorProvider1.Clear();
                //}

                //if (regex3.IsMatch(textBox3.Text) == false)
                //{
                //    textBox3.Focus();
                //    errorProvider1.SetError(textBox3, "Only numbers may be entered here");

                //}
                //else
                //{
                //    errorProvider1.Clear();
                //}

                //if (regex2.IsMatch(textBox5.Text) == false)
                //{
                //    textBox5.Focus();
                //    errorProvider1.SetError(textBox5, "Only numbers may be entered here");

                //}
                //else
                //{
                //    errorProvider1.Clear();
                //}
            }


              


                

            
           
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void Student_Operations_Load(object sender, EventArgs e)
        {

        }
    }
}
