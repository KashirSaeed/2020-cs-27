﻿
namespace DB2020_CS_27
{
    partial class deleteAndUpdateRubricLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.updateAssessment = new System.Windows.Forms.Button();
            this.deleteAssessment = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.rubricLevelID = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.warningLable = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(286, 31);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 38;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // updateAssessment
            // 
            this.updateAssessment.Location = new System.Drawing.Point(464, 427);
            this.updateAssessment.Name = "updateAssessment";
            this.updateAssessment.Size = new System.Drawing.Size(150, 53);
            this.updateAssessment.TabIndex = 37;
            this.updateAssessment.Text = "Update Assessment";
            this.updateAssessment.UseVisualStyleBackColor = true;
            this.updateAssessment.Click += new System.EventHandler(this.updateAssessment_Click);
            // 
            // deleteAssessment
            // 
            this.deleteAssessment.Location = new System.Drawing.Point(620, 427);
            this.deleteAssessment.Name = "deleteAssessment";
            this.deleteAssessment.Size = new System.Drawing.Size(150, 53);
            this.deleteAssessment.TabIndex = 36;
            this.deleteAssessment.Text = "Delete Assessment";
            this.deleteAssessment.UseVisualStyleBackColor = true;
            this.deleteAssessment.Click += new System.EventHandler(this.deleteAssessment_Click);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(35, 84);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 35;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // rubricLevelID
            // 
            this.rubricLevelID.AutoSize = true;
            this.rubricLevelID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricLevelID.Location = new System.Drawing.Point(30, 31);
            this.rubricLevelID.Name = "rubricLevelID";
            this.rubricLevelID.Size = new System.Drawing.Size(143, 25);
            this.rubricLevelID.TabIndex = 34;
            this.rubricLevelID.Text = "Rubric Level ID";
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(120, 169);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 33;
            this.update_And_Delete_Table.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.update_And_Delete_Table_DataError);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1060, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 25);
            this.label2.TabIndex = 40;
            this.label2.Text = "Rubric ID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(830, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 25);
            this.label1.TabIndex = 39;
            this.label1.Text = "Rubric Level Detail:  ";
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(227, 364);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(535, 25);
            this.warningLable.TabIndex = 41;
            this.warningLable.Text = "This Rubric id is not added. Please enter the correct one !!!!!!";
            // 
            // deleteAndUpdateRubricLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1317, 528);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateAssessment);
            this.Controls.Add(this.deleteAssessment);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.rubricLevelID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "deleteAndUpdateRubricLevel";
            this.Text = "deleteAndUpdateRubricLevel";
            this.Load += new System.EventHandler(this.deleteAndUpdateRubricLevel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button updateAssessment;
        private System.Windows.Forms.Button deleteAssessment;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label rubricLevelID;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label warningLable;
    }
}