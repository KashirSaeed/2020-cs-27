﻿
namespace DB2020_CS_27
{
    partial class deleteAndUpdateRubric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.updateAssessment = new System.Windows.Forms.Button();
            this.deleteAssessment = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.rubricID = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.warningLable = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(286, 31);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 32;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // updateAssessment
            // 
            this.updateAssessment.Location = new System.Drawing.Point(464, 407);
            this.updateAssessment.Name = "updateAssessment";
            this.updateAssessment.Size = new System.Drawing.Size(150, 53);
            this.updateAssessment.TabIndex = 31;
            this.updateAssessment.Text = "Update Assessment";
            this.updateAssessment.UseVisualStyleBackColor = true;
            this.updateAssessment.Click += new System.EventHandler(this.updateAssessment_Click);
            // 
            // deleteAssessment
            // 
            this.deleteAssessment.Location = new System.Drawing.Point(620, 407);
            this.deleteAssessment.Name = "deleteAssessment";
            this.deleteAssessment.Size = new System.Drawing.Size(150, 53);
            this.deleteAssessment.TabIndex = 30;
            this.deleteAssessment.Text = "Delete Assessment";
            this.deleteAssessment.UseVisualStyleBackColor = true;
            this.deleteAssessment.Click += new System.EventHandler(this.deleteAssessment_Click);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(35, 73);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 29;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // rubricID
            // 
            this.rubricID.AutoSize = true;
            this.rubricID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricID.Location = new System.Drawing.Point(30, 31);
            this.rubricID.Name = "rubricID";
            this.rubricID.Size = new System.Drawing.Size(91, 25);
            this.rubricID.TabIndex = 28;
            this.rubricID.Text = "Rubric ID";
            this.rubricID.Click += new System.EventHandler(this.rubricID_Click);
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(120, 143);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 27;
            this.update_And_Delete_Table.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.update_And_Delete_Table_CellContentClick);
            this.update_And_Delete_Table.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.update_And_Delete_Table_DataError);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(778, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 25);
            this.label1.TabIndex = 33;
            this.label1.Text = "Rubric Detail";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(932, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 25);
            this.label2.TabIndex = 34;
            this.label2.Text = "Rubric ID";
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(230, 342);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(522, 25);
            this.warningLable.TabIndex = 35;
            this.warningLable.Text = "This CLO id is not added. Please enter the correct one !!!!!!";
            // 
            // deleteAndUpdateRubric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1311, 503);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateAssessment);
            this.Controls.Add(this.deleteAssessment);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.rubricID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "deleteAndUpdateRubric";
            this.Text = "deleteAndUpdateRubric";
            this.Load += new System.EventHandler(this.deleteAndUpdateRubric_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button updateAssessment;
        private System.Windows.Forms.Button deleteAssessment;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label rubricID;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label warningLable;
    }
}