﻿
namespace DB2020_CS_27
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tittle = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.deleteAndUpdateStudentAttendence = new System.Windows.Forms.Button();
            this.deleteAndUpdateStudentResult = new System.Windows.Forms.Button();
            this.deleteAndUpdateRubricLevel = new System.Windows.Forms.Button();
            this.deleteAndUpdateRubric = new System.Windows.Forms.Button();
            this.displayStudentResult = new System.Windows.Forms.Button();
            this.displayRubricLevel = new System.Windows.Forms.Button();
            this.displayRubrics = new System.Windows.Forms.Button();
            this.displayStudentAttendence = new System.Windows.Forms.Button();
            this.studentResult = new System.Windows.Forms.Button();
            this.subPanel5 = new System.Windows.Forms.Panel();
            this.displayAssessmentComponent = new System.Windows.Forms.Button();
            this.updateAssessmentComponent = new System.Windows.Forms.Button();
            this.addAssessmentComponent = new System.Windows.Forms.Button();
            this.assessmentComponent = new System.Windows.Forms.Button();
            this.rubricLevel = new System.Windows.Forms.Button();
            this.rubric = new System.Windows.Forms.Button();
            this.studentAttendence = new System.Windows.Forms.Button();
            this.subPanel4 = new System.Windows.Forms.Panel();
            this.deleteAndUpdateAttendenceDate = new System.Windows.Forms.Button();
            this.displayAttendenceDate = new System.Windows.Forms.Button();
            this.addAttendenceDate = new System.Windows.Forms.Button();
            this.classAttendence = new System.Windows.Forms.Button();
            this.subPanel3 = new System.Windows.Forms.Panel();
            this.addCLO = new System.Windows.Forms.Button();
            this.displayCLO = new System.Windows.Forms.Button();
            this.deleteAndUpdateCLO = new System.Windows.Forms.Button();
            this.CLO = new System.Windows.Forms.Button();
            this.subPanel2 = new System.Windows.Forms.Panel();
            this.delete_And_Update_Assessment = new System.Windows.Forms.Button();
            this.dispalyAssessment = new System.Windows.Forms.Button();
            this.addAssessment = new System.Windows.Forms.Button();
            this.assessment = new System.Windows.Forms.Button();
            this.subPanel1 = new System.Windows.Forms.Panel();
            this.delete_And_Update_Student = new System.Windows.Forms.Button();
            this.displayStudent = new System.Windows.Forms.Button();
            this.addStudent = new System.Windows.Forms.Button();
            this.studentButton = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.PDF_Generator_Buttton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.subPanel5.SuspendLayout();
            this.subPanel4.SuspendLayout();
            this.subPanel3.SuspendLayout();
            this.subPanel2.SuspendLayout();
            this.subPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel1.Controls.Add(this.tittle);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1362, 100);
            this.panel1.TabIndex = 0;
            // 
            // tittle
            // 
            this.tittle.AutoSize = true;
            this.tittle.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tittle.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.tittle.Location = new System.Drawing.Point(309, 16);
            this.tittle.Name = "tittle";
            this.tittle.Size = new System.Drawing.Size(821, 69);
            this.tittle.TabIndex = 1;
            this.tittle.Text = "Project Management System";
            this.tittle.Click += new System.EventHandler(this.tittle_Click);
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel4.Location = new System.Drawing.Point(0, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 91);
            this.panel4.TabIndex = 0;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.BackColor = System.Drawing.SystemColors.Window;
            this.panel2.Controls.Add(this.PDF_Generator_Buttton);
            this.panel2.Controls.Add(this.deleteAndUpdateStudentAttendence);
            this.panel2.Controls.Add(this.deleteAndUpdateStudentResult);
            this.panel2.Controls.Add(this.deleteAndUpdateRubricLevel);
            this.panel2.Controls.Add(this.deleteAndUpdateRubric);
            this.panel2.Controls.Add(this.displayStudentResult);
            this.panel2.Controls.Add(this.displayRubricLevel);
            this.panel2.Controls.Add(this.displayRubrics);
            this.panel2.Controls.Add(this.displayStudentAttendence);
            this.panel2.Controls.Add(this.studentResult);
            this.panel2.Controls.Add(this.subPanel5);
            this.panel2.Controls.Add(this.assessmentComponent);
            this.panel2.Controls.Add(this.rubricLevel);
            this.panel2.Controls.Add(this.rubric);
            this.panel2.Controls.Add(this.studentAttendence);
            this.panel2.Controls.Add(this.subPanel4);
            this.panel2.Controls.Add(this.classAttendence);
            this.panel2.Controls.Add(this.subPanel3);
            this.panel2.Controls.Add(this.CLO);
            this.panel2.Controls.Add(this.subPanel2);
            this.panel2.Controls.Add(this.assessment);
            this.panel2.Controls.Add(this.subPanel1);
            this.panel2.Controls.Add(this.studentButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 636);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // deleteAndUpdateStudentAttendence
            // 
            this.deleteAndUpdateStudentAttendence.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteAndUpdateStudentAttendence.Location = new System.Drawing.Point(0, 1183);
            this.deleteAndUpdateStudentAttendence.Name = "deleteAndUpdateStudentAttendence";
            this.deleteAndUpdateStudentAttendence.Size = new System.Drawing.Size(179, 53);
            this.deleteAndUpdateStudentAttendence.TabIndex = 25;
            this.deleteAndUpdateStudentAttendence.Text = "Delete And Update Student Attendence";
            this.deleteAndUpdateStudentAttendence.UseVisualStyleBackColor = true;
            this.deleteAndUpdateStudentAttendence.Click += new System.EventHandler(this.deleteAndUpdateStudentAttendence_Click);
            // 
            // deleteAndUpdateStudentResult
            // 
            this.deleteAndUpdateStudentResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteAndUpdateStudentResult.Location = new System.Drawing.Point(0, 1137);
            this.deleteAndUpdateStudentResult.Name = "deleteAndUpdateStudentResult";
            this.deleteAndUpdateStudentResult.Size = new System.Drawing.Size(179, 46);
            this.deleteAndUpdateStudentResult.TabIndex = 24;
            this.deleteAndUpdateStudentResult.Text = "Delete And Update StudentResult";
            this.deleteAndUpdateStudentResult.UseVisualStyleBackColor = true;
            this.deleteAndUpdateStudentResult.Click += new System.EventHandler(this.deleteAndUpdateStudentResult_Click);
            // 
            // deleteAndUpdateRubricLevel
            // 
            this.deleteAndUpdateRubricLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteAndUpdateRubricLevel.Location = new System.Drawing.Point(0, 1091);
            this.deleteAndUpdateRubricLevel.Name = "deleteAndUpdateRubricLevel";
            this.deleteAndUpdateRubricLevel.Size = new System.Drawing.Size(179, 46);
            this.deleteAndUpdateRubricLevel.TabIndex = 23;
            this.deleteAndUpdateRubricLevel.Text = "Delete And Update RubricLevel";
            this.deleteAndUpdateRubricLevel.UseVisualStyleBackColor = true;
            this.deleteAndUpdateRubricLevel.Click += new System.EventHandler(this.deleteAndUpdateRubricLevel_Click);
            // 
            // deleteAndUpdateRubric
            // 
            this.deleteAndUpdateRubric.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteAndUpdateRubric.Location = new System.Drawing.Point(0, 1047);
            this.deleteAndUpdateRubric.Name = "deleteAndUpdateRubric";
            this.deleteAndUpdateRubric.Size = new System.Drawing.Size(179, 44);
            this.deleteAndUpdateRubric.TabIndex = 22;
            this.deleteAndUpdateRubric.Text = "Delete And Update Rubric";
            this.deleteAndUpdateRubric.UseVisualStyleBackColor = true;
            this.deleteAndUpdateRubric.Click += new System.EventHandler(this.deleteAndUpdateRubric_Click);
            // 
            // displayStudentResult
            // 
            this.displayStudentResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayStudentResult.Location = new System.Drawing.Point(0, 1013);
            this.displayStudentResult.Name = "displayStudentResult";
            this.displayStudentResult.Size = new System.Drawing.Size(179, 34);
            this.displayStudentResult.TabIndex = 21;
            this.displayStudentResult.Text = "Display Student Result";
            this.displayStudentResult.UseVisualStyleBackColor = true;
            this.displayStudentResult.Click += new System.EventHandler(this.displayStudentResult_Click);
            // 
            // displayRubricLevel
            // 
            this.displayRubricLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayRubricLevel.Location = new System.Drawing.Point(0, 979);
            this.displayRubricLevel.Name = "displayRubricLevel";
            this.displayRubricLevel.Size = new System.Drawing.Size(179, 34);
            this.displayRubricLevel.TabIndex = 20;
            this.displayRubricLevel.Text = "Display Rubric Level";
            this.displayRubricLevel.UseVisualStyleBackColor = true;
            this.displayRubricLevel.Click += new System.EventHandler(this.displayRubricLevel_Click);
            // 
            // displayRubrics
            // 
            this.displayRubrics.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayRubrics.Location = new System.Drawing.Point(0, 945);
            this.displayRubrics.Name = "displayRubrics";
            this.displayRubrics.Size = new System.Drawing.Size(179, 34);
            this.displayRubrics.TabIndex = 19;
            this.displayRubrics.Text = "Display Rubrics";
            this.displayRubrics.UseVisualStyleBackColor = true;
            this.displayRubrics.Click += new System.EventHandler(this.displayRubrics_Click);
            // 
            // displayStudentAttendence
            // 
            this.displayStudentAttendence.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayStudentAttendence.Location = new System.Drawing.Point(0, 911);
            this.displayStudentAttendence.Name = "displayStudentAttendence";
            this.displayStudentAttendence.Size = new System.Drawing.Size(179, 34);
            this.displayStudentAttendence.TabIndex = 18;
            this.displayStudentAttendence.Text = "Display Student Attendence";
            this.displayStudentAttendence.UseVisualStyleBackColor = true;
            this.displayStudentAttendence.Click += new System.EventHandler(this.displayStudentAttendence_Click);
            // 
            // studentResult
            // 
            this.studentResult.Dock = System.Windows.Forms.DockStyle.Top;
            this.studentResult.Location = new System.Drawing.Point(0, 877);
            this.studentResult.Name = "studentResult";
            this.studentResult.Size = new System.Drawing.Size(179, 34);
            this.studentResult.TabIndex = 17;
            this.studentResult.Text = "Student Result";
            this.studentResult.UseVisualStyleBackColor = true;
            this.studentResult.Click += new System.EventHandler(this.studentResult_Click_1);
            // 
            // subPanel5
            // 
            this.subPanel5.Controls.Add(this.displayAssessmentComponent);
            this.subPanel5.Controls.Add(this.updateAssessmentComponent);
            this.subPanel5.Controls.Add(this.addAssessmentComponent);
            this.subPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanel5.Location = new System.Drawing.Point(0, 745);
            this.subPanel5.Name = "subPanel5";
            this.subPanel5.Size = new System.Drawing.Size(179, 132);
            this.subPanel5.TabIndex = 9;
            this.subPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.subPanel5_Paint);
            // 
            // displayAssessmentComponent
            // 
            this.displayAssessmentComponent.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayAssessmentComponent.Location = new System.Drawing.Point(0, 86);
            this.displayAssessmentComponent.Name = "displayAssessmentComponent";
            this.displayAssessmentComponent.Size = new System.Drawing.Size(179, 42);
            this.displayAssessmentComponent.TabIndex = 15;
            this.displayAssessmentComponent.Text = "Display Assessment Component";
            this.displayAssessmentComponent.UseVisualStyleBackColor = true;
            this.displayAssessmentComponent.Click += new System.EventHandler(this.displayAssessmentComponent_Click);
            // 
            // updateAssessmentComponent
            // 
            this.updateAssessmentComponent.Dock = System.Windows.Forms.DockStyle.Top;
            this.updateAssessmentComponent.Location = new System.Drawing.Point(0, 42);
            this.updateAssessmentComponent.Name = "updateAssessmentComponent";
            this.updateAssessmentComponent.Size = new System.Drawing.Size(179, 44);
            this.updateAssessmentComponent.TabIndex = 13;
            this.updateAssessmentComponent.Text = "Update Assessment Component";
            this.updateAssessmentComponent.UseVisualStyleBackColor = true;
            this.updateAssessmentComponent.Click += new System.EventHandler(this.updateAssessmentComponent_Click);
            // 
            // addAssessmentComponent
            // 
            this.addAssessmentComponent.Dock = System.Windows.Forms.DockStyle.Top;
            this.addAssessmentComponent.Location = new System.Drawing.Point(0, 0);
            this.addAssessmentComponent.Name = "addAssessmentComponent";
            this.addAssessmentComponent.Size = new System.Drawing.Size(179, 42);
            this.addAssessmentComponent.TabIndex = 12;
            this.addAssessmentComponent.Text = "Add Assessment Component";
            this.addAssessmentComponent.UseVisualStyleBackColor = true;
            this.addAssessmentComponent.Click += new System.EventHandler(this.addAssessmentComponent_Click);
            // 
            // assessmentComponent
            // 
            this.assessmentComponent.Dock = System.Windows.Forms.DockStyle.Top;
            this.assessmentComponent.Location = new System.Drawing.Point(0, 711);
            this.assessmentComponent.Name = "assessmentComponent";
            this.assessmentComponent.Size = new System.Drawing.Size(179, 34);
            this.assessmentComponent.TabIndex = 11;
            this.assessmentComponent.Text = "Assessment Component";
            this.assessmentComponent.UseVisualStyleBackColor = true;
            this.assessmentComponent.Click += new System.EventHandler(this.assessmentComponent_Click);
            // 
            // rubricLevel
            // 
            this.rubricLevel.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubricLevel.Location = new System.Drawing.Point(0, 677);
            this.rubricLevel.Name = "rubricLevel";
            this.rubricLevel.Size = new System.Drawing.Size(179, 34);
            this.rubricLevel.TabIndex = 10;
            this.rubricLevel.Text = "Rubric Level";
            this.rubricLevel.UseVisualStyleBackColor = true;
            this.rubricLevel.Click += new System.EventHandler(this.rubricLevel_Click);
            // 
            // rubric
            // 
            this.rubric.Dock = System.Windows.Forms.DockStyle.Top;
            this.rubric.Location = new System.Drawing.Point(0, 643);
            this.rubric.Name = "rubric";
            this.rubric.Size = new System.Drawing.Size(179, 34);
            this.rubric.TabIndex = 9;
            this.rubric.Text = "Rubrics ";
            this.rubric.UseVisualStyleBackColor = true;
            this.rubric.Click += new System.EventHandler(this.rubric_Click);
            // 
            // studentAttendence
            // 
            this.studentAttendence.Dock = System.Windows.Forms.DockStyle.Top;
            this.studentAttendence.Location = new System.Drawing.Point(0, 609);
            this.studentAttendence.Name = "studentAttendence";
            this.studentAttendence.Size = new System.Drawing.Size(179, 34);
            this.studentAttendence.TabIndex = 8;
            this.studentAttendence.Text = "Student Attendence";
            this.studentAttendence.UseVisualStyleBackColor = true;
            this.studentAttendence.Click += new System.EventHandler(this.studentAttendence_Click);
            // 
            // subPanel4
            // 
            this.subPanel4.Controls.Add(this.deleteAndUpdateAttendenceDate);
            this.subPanel4.Controls.Add(this.displayAttendenceDate);
            this.subPanel4.Controls.Add(this.addAttendenceDate);
            this.subPanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanel4.Location = new System.Drawing.Point(0, 461);
            this.subPanel4.Name = "subPanel4";
            this.subPanel4.Size = new System.Drawing.Size(179, 148);
            this.subPanel4.TabIndex = 7;
            // 
            // deleteAndUpdateAttendenceDate
            // 
            this.deleteAndUpdateAttendenceDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteAndUpdateAttendenceDate.Location = new System.Drawing.Point(0, 68);
            this.deleteAndUpdateAttendenceDate.Name = "deleteAndUpdateAttendenceDate";
            this.deleteAndUpdateAttendenceDate.Size = new System.Drawing.Size(179, 45);
            this.deleteAndUpdateAttendenceDate.TabIndex = 8;
            this.deleteAndUpdateAttendenceDate.Text = "Delete and Update AttendenceDate";
            this.deleteAndUpdateAttendenceDate.UseVisualStyleBackColor = true;
            this.deleteAndUpdateAttendenceDate.Click += new System.EventHandler(this.deleteAndUpdateAttendenceDate_Click);
            // 
            // displayAttendenceDate
            // 
            this.displayAttendenceDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayAttendenceDate.Location = new System.Drawing.Point(0, 34);
            this.displayAttendenceDate.Name = "displayAttendenceDate";
            this.displayAttendenceDate.Size = new System.Drawing.Size(179, 34);
            this.displayAttendenceDate.TabIndex = 7;
            this.displayAttendenceDate.Text = "Display Attendence Date";
            this.displayAttendenceDate.UseVisualStyleBackColor = true;
            this.displayAttendenceDate.Click += new System.EventHandler(this.displayAttendenceDate_Click);
            // 
            // addAttendenceDate
            // 
            this.addAttendenceDate.Dock = System.Windows.Forms.DockStyle.Top;
            this.addAttendenceDate.Location = new System.Drawing.Point(0, 0);
            this.addAttendenceDate.Name = "addAttendenceDate";
            this.addAttendenceDate.Size = new System.Drawing.Size(179, 34);
            this.addAttendenceDate.TabIndex = 6;
            this.addAttendenceDate.Text = "Add Attendence Date";
            this.addAttendenceDate.UseVisualStyleBackColor = true;
            this.addAttendenceDate.Click += new System.EventHandler(this.addAttendenceDate_Click);
            // 
            // classAttendence
            // 
            this.classAttendence.Dock = System.Windows.Forms.DockStyle.Top;
            this.classAttendence.Location = new System.Drawing.Point(0, 427);
            this.classAttendence.Name = "classAttendence";
            this.classAttendence.Size = new System.Drawing.Size(179, 34);
            this.classAttendence.TabIndex = 6;
            this.classAttendence.Text = "Class Attendence";
            this.classAttendence.UseVisualStyleBackColor = true;
            this.classAttendence.Click += new System.EventHandler(this.classAttendence_Click);
            // 
            // subPanel3
            // 
            this.subPanel3.Controls.Add(this.addCLO);
            this.subPanel3.Controls.Add(this.displayCLO);
            this.subPanel3.Controls.Add(this.deleteAndUpdateCLO);
            this.subPanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanel3.Location = new System.Drawing.Point(0, 321);
            this.subPanel3.Name = "subPanel3";
            this.subPanel3.Size = new System.Drawing.Size(179, 106);
            this.subPanel3.TabIndex = 5;
            // 
            // addCLO
            // 
            this.addCLO.Dock = System.Windows.Forms.DockStyle.Top;
            this.addCLO.Location = new System.Drawing.Point(0, 68);
            this.addCLO.Name = "addCLO";
            this.addCLO.Size = new System.Drawing.Size(179, 34);
            this.addCLO.TabIndex = 5;
            this.addCLO.Text = "Add CLO";
            this.addCLO.UseVisualStyleBackColor = true;
            this.addCLO.Click += new System.EventHandler(this.addCLO_Click_1);
            // 
            // displayCLO
            // 
            this.displayCLO.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayCLO.Location = new System.Drawing.Point(0, 34);
            this.displayCLO.Name = "displayCLO";
            this.displayCLO.Size = new System.Drawing.Size(179, 34);
            this.displayCLO.TabIndex = 6;
            this.displayCLO.Text = "Display CLO";
            this.displayCLO.UseVisualStyleBackColor = true;
            this.displayCLO.Click += new System.EventHandler(this.displayCLO_Click);
            // 
            // deleteAndUpdateCLO
            // 
            this.deleteAndUpdateCLO.Dock = System.Windows.Forms.DockStyle.Top;
            this.deleteAndUpdateCLO.Location = new System.Drawing.Point(0, 0);
            this.deleteAndUpdateCLO.Name = "deleteAndUpdateCLO";
            this.deleteAndUpdateCLO.Size = new System.Drawing.Size(179, 34);
            this.deleteAndUpdateCLO.TabIndex = 7;
            this.deleteAndUpdateCLO.Text = "delete_and_Update_CLO";
            this.deleteAndUpdateCLO.UseVisualStyleBackColor = true;
            this.deleteAndUpdateCLO.Click += new System.EventHandler(this.deleteAndUpdateCLO_Click);
            // 
            // CLO
            // 
            this.CLO.Dock = System.Windows.Forms.DockStyle.Top;
            this.CLO.Location = new System.Drawing.Point(0, 287);
            this.CLO.Name = "CLO";
            this.CLO.Size = new System.Drawing.Size(179, 34);
            this.CLO.TabIndex = 4;
            this.CLO.Text = "CLO";
            this.CLO.UseVisualStyleBackColor = true;
            this.CLO.Click += new System.EventHandler(this.CLO_Click);
            // 
            // subPanel2
            // 
            this.subPanel2.Controls.Add(this.delete_And_Update_Assessment);
            this.subPanel2.Controls.Add(this.dispalyAssessment);
            this.subPanel2.Controls.Add(this.addAssessment);
            this.subPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanel2.Location = new System.Drawing.Point(0, 171);
            this.subPanel2.Name = "subPanel2";
            this.subPanel2.Size = new System.Drawing.Size(179, 116);
            this.subPanel2.TabIndex = 3;
            // 
            // delete_And_Update_Assessment
            // 
            this.delete_And_Update_Assessment.Dock = System.Windows.Forms.DockStyle.Top;
            this.delete_And_Update_Assessment.Location = new System.Drawing.Point(0, 68);
            this.delete_And_Update_Assessment.Name = "delete_And_Update_Assessment";
            this.delete_And_Update_Assessment.Size = new System.Drawing.Size(179, 46);
            this.delete_And_Update_Assessment.TabIndex = 4;
            this.delete_And_Update_Assessment.Text = "Delete And Update Assessment";
            this.delete_And_Update_Assessment.UseVisualStyleBackColor = true;
            this.delete_And_Update_Assessment.Click += new System.EventHandler(this.delete_And_Update_Assessment_Click);
            // 
            // dispalyAssessment
            // 
            this.dispalyAssessment.Dock = System.Windows.Forms.DockStyle.Top;
            this.dispalyAssessment.Location = new System.Drawing.Point(0, 34);
            this.dispalyAssessment.Name = "dispalyAssessment";
            this.dispalyAssessment.Size = new System.Drawing.Size(179, 34);
            this.dispalyAssessment.TabIndex = 3;
            this.dispalyAssessment.Text = "Display Assessment";
            this.dispalyAssessment.UseVisualStyleBackColor = true;
            this.dispalyAssessment.Click += new System.EventHandler(this.dispalyAssessment_Click);
            // 
            // addAssessment
            // 
            this.addAssessment.Dock = System.Windows.Forms.DockStyle.Top;
            this.addAssessment.Location = new System.Drawing.Point(0, 0);
            this.addAssessment.Name = "addAssessment";
            this.addAssessment.Size = new System.Drawing.Size(179, 34);
            this.addAssessment.TabIndex = 2;
            this.addAssessment.Text = "Add Assessment";
            this.addAssessment.UseVisualStyleBackColor = true;
            this.addAssessment.Click += new System.EventHandler(this.addAssessment_Click);
            // 
            // assessment
            // 
            this.assessment.Dock = System.Windows.Forms.DockStyle.Top;
            this.assessment.Location = new System.Drawing.Point(0, 137);
            this.assessment.Name = "assessment";
            this.assessment.Size = new System.Drawing.Size(179, 34);
            this.assessment.TabIndex = 2;
            this.assessment.Text = "Assessment";
            this.assessment.UseVisualStyleBackColor = true;
            this.assessment.Click += new System.EventHandler(this.assessment_Click);
            // 
            // subPanel1
            // 
            this.subPanel1.Controls.Add(this.delete_And_Update_Student);
            this.subPanel1.Controls.Add(this.displayStudent);
            this.subPanel1.Controls.Add(this.addStudent);
            this.subPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.subPanel1.Location = new System.Drawing.Point(0, 34);
            this.subPanel1.Name = "subPanel1";
            this.subPanel1.Size = new System.Drawing.Size(179, 103);
            this.subPanel1.TabIndex = 1;
            // 
            // delete_And_Update_Student
            // 
            this.delete_And_Update_Student.Dock = System.Windows.Forms.DockStyle.Top;
            this.delete_And_Update_Student.Location = new System.Drawing.Point(0, 68);
            this.delete_And_Update_Student.Name = "delete_And_Update_Student";
            this.delete_And_Update_Student.Size = new System.Drawing.Size(179, 34);
            this.delete_And_Update_Student.TabIndex = 3;
            this.delete_And_Update_Student.Text = "Delete / Update Student";
            this.delete_And_Update_Student.UseVisualStyleBackColor = true;
            this.delete_And_Update_Student.Click += new System.EventHandler(this.delete_And_Update_Student_Click);
            // 
            // displayStudent
            // 
            this.displayStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.displayStudent.Location = new System.Drawing.Point(0, 34);
            this.displayStudent.Name = "displayStudent";
            this.displayStudent.Size = new System.Drawing.Size(179, 34);
            this.displayStudent.TabIndex = 2;
            this.displayStudent.Text = "Display Student";
            this.displayStudent.UseVisualStyleBackColor = true;
            this.displayStudent.Click += new System.EventHandler(this.displayStudent_Click);
            // 
            // addStudent
            // 
            this.addStudent.Dock = System.Windows.Forms.DockStyle.Top;
            this.addStudent.Location = new System.Drawing.Point(0, 0);
            this.addStudent.Name = "addStudent";
            this.addStudent.Size = new System.Drawing.Size(179, 34);
            this.addStudent.TabIndex = 1;
            this.addStudent.Text = "Add Student";
            this.addStudent.UseVisualStyleBackColor = true;
            this.addStudent.Click += new System.EventHandler(this.addStudent_Click);
            // 
            // studentButton
            // 
            this.studentButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.studentButton.Location = new System.Drawing.Point(0, 0);
            this.studentButton.Name = "studentButton";
            this.studentButton.Size = new System.Drawing.Size(179, 34);
            this.studentButton.TabIndex = 0;
            this.studentButton.Text = "Student";
            this.studentButton.UseVisualStyleBackColor = true;
            this.studentButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.mainPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mainPanel.BackgroundImage")));
            this.mainPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(200, 100);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(1162, 636);
            this.mainPanel.TabIndex = 2;
            this.mainPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // PDF_Generator_Buttton
            // 
            this.PDF_Generator_Buttton.Dock = System.Windows.Forms.DockStyle.Top;
            this.PDF_Generator_Buttton.Location = new System.Drawing.Point(0, 1236);
            this.PDF_Generator_Buttton.Name = "PDF_Generator_Buttton";
            this.PDF_Generator_Buttton.Size = new System.Drawing.Size(179, 34);
            this.PDF_Generator_Buttton.TabIndex = 26;
            this.PDF_Generator_Buttton.Text = "PDF Generator";
            this.PDF_Generator_Buttton.UseVisualStyleBackColor = true;
            this.PDF_Generator_Buttton.Click += new System.EventHandler(this.PDF_Generator_Buttton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 736);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "mainPage";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.subPanel5.ResumeLayout(false);
            this.subPanel4.ResumeLayout(false);
            this.subPanel3.ResumeLayout(false);
            this.subPanel2.ResumeLayout(false);
            this.subPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label tittle;
        private System.Windows.Forms.Panel subPanel1;
        private System.Windows.Forms.Button delete_And_Update_Student;
        private System.Windows.Forms.Button displayStudent;
        private System.Windows.Forms.Button addStudent;
        private System.Windows.Forms.Button studentButton;
        private System.Windows.Forms.Panel subPanel2;
        private System.Windows.Forms.Button delete_And_Update_Assessment;
        private System.Windows.Forms.Button dispalyAssessment;
        private System.Windows.Forms.Button addAssessment;
        private System.Windows.Forms.Button assessment;
        private System.Windows.Forms.Button CLO;
        private System.Windows.Forms.Panel subPanel3;
        private System.Windows.Forms.Button addCLO;
        private System.Windows.Forms.Button displayCLO;
        private System.Windows.Forms.Button deleteAndUpdateCLO;
        private System.Windows.Forms.Panel subPanel4;
        private System.Windows.Forms.Button deleteAndUpdateAttendenceDate;
        private System.Windows.Forms.Button displayAttendenceDate;
        private System.Windows.Forms.Button addAttendenceDate;
        private System.Windows.Forms.Button classAttendence;
        private System.Windows.Forms.Button studentAttendence;
        private System.Windows.Forms.Button rubric;
        private System.Windows.Forms.Button rubricLevel;
        private System.Windows.Forms.Panel subPanel5;
        private System.Windows.Forms.Button displayAssessmentComponent;
        private System.Windows.Forms.Button updateAssessmentComponent;
        private System.Windows.Forms.Button addAssessmentComponent;
        private System.Windows.Forms.Button assessmentComponent;
        private System.Windows.Forms.Button studentResult;
        private System.Windows.Forms.Button displayStudentAttendence;
        private System.Windows.Forms.Button displayStudentResult;
        private System.Windows.Forms.Button displayRubricLevel;
        private System.Windows.Forms.Button displayRubrics;
        private System.Windows.Forms.Button deleteAndUpdateStudentAttendence;
        private System.Windows.Forms.Button deleteAndUpdateStudentResult;
        private System.Windows.Forms.Button deleteAndUpdateRubricLevel;
        private System.Windows.Forms.Button deleteAndUpdateRubric;
        private System.Windows.Forms.Button PDF_Generator_Buttton;
    }
}

