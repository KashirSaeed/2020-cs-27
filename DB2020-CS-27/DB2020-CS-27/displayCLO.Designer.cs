﻿
namespace DB2020_CS_27
{
    partial class displayCLO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.disCLO = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.disCLO)).BeginInit();
            this.SuspendLayout();
            // 
            // disCLO
            // 
            this.disCLO.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.disCLO.Location = new System.Drawing.Point(40, 97);
            this.disCLO.Name = "disCLO";
            this.disCLO.RowHeadersWidth = 51;
            this.disCLO.RowTemplate.Height = 24;
            this.disCLO.Size = new System.Drawing.Size(704, 269);
            this.disCLO.TabIndex = 1;
            // 
            // displayCLO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 450);
            this.Controls.Add(this.disCLO);
            this.Name = "displayCLO";
            this.Text = "displayCLO";
            this.Load += new System.EventHandler(this.displayCLO_Load);
            ((System.ComponentModel.ISupportInitialize)(this.disCLO)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView disCLO;
    }
}