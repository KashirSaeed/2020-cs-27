﻿
namespace DB2020_CS_27
{
    partial class addCLO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.addCLOButton = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dateUpdated = new System.Windows.Forms.Label();
            this.dateCreated = new System.Windows.Forms.Label();
            this.namCLO = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.warningLable = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(403, 125);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(273, 22);
            this.dateTimePicker1.TabIndex = 19;
            // 
            // addCLOButton
            // 
            this.addCLOButton.Location = new System.Drawing.Point(504, 255);
            this.addCLOButton.Name = "addCLOButton";
            this.addCLOButton.Size = new System.Drawing.Size(172, 51);
            this.addCLOButton.TabIndex = 18;
            this.addCLOButton.Text = "Add CLO";
            this.addCLOButton.UseVisualStyleBackColor = true;
            this.addCLOButton.Click += new System.EventHandler(this.addCLOButton_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(403, 78);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(273, 22);
            this.textBox1.TabIndex = 15;
            // 
            // dateUpdated
            // 
            this.dateUpdated.AutoSize = true;
            this.dateUpdated.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateUpdated.Location = new System.Drawing.Point(125, 181);
            this.dateUpdated.Name = "dateUpdated";
            this.dateUpdated.Size = new System.Drawing.Size(161, 29);
            this.dateUpdated.TabIndex = 13;
            this.dateUpdated.Text = "Date Updated";
            // 
            // dateCreated
            // 
            this.dateCreated.AutoSize = true;
            this.dateCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateCreated.Location = new System.Drawing.Point(125, 125);
            this.dateCreated.Name = "dateCreated";
            this.dateCreated.Size = new System.Drawing.Size(155, 29);
            this.dateCreated.TabIndex = 12;
            this.dateCreated.Text = "Date Created";
            // 
            // namCLO
            // 
            this.namCLO.AutoSize = true;
            this.namCLO.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.namCLO.Location = new System.Drawing.Point(125, 71);
            this.namCLO.Name = "namCLO";
            this.namCLO.Size = new System.Drawing.Size(139, 29);
            this.namCLO.TabIndex = 11;
            this.namCLO.Text = "CLO Name ";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(403, 181);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(273, 22);
            this.dateTimePicker2.TabIndex = 20;
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(306, 226);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(195, 17);
            this.warningLable.TabIndex = 21;
            this.warningLable.Text = "Please enter in correct format";
            // 
            // addCLO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.addCLOButton);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.dateUpdated);
            this.Controls.Add(this.dateCreated);
            this.Controls.Add(this.namCLO);
            this.Name = "addCLO";
            this.Text = "addCLO";
            this.Load += new System.EventHandler(this.addCLO_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button addCLOButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label dateUpdated;
        private System.Windows.Forms.Label dateCreated;
        private System.Windows.Forms.Label namCLO;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label warningLable;
    }
}