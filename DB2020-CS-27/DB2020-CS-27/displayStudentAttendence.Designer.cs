﻿
namespace DB2020_CS_27
{
    partial class displayStudentAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayAss = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.displayAss)).BeginInit();
            this.SuspendLayout();
            // 
            // displayAss
            // 
            this.displayAss.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.displayAss.Location = new System.Drawing.Point(56, 90);
            this.displayAss.Name = "displayAss";
            this.displayAss.RowHeadersWidth = 51;
            this.displayAss.RowTemplate.Height = 24;
            this.displayAss.Size = new System.Drawing.Size(638, 198);
            this.displayAss.TabIndex = 1;
            // 
            // displayStudentAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1001, 450);
            this.Controls.Add(this.displayAss);
            this.Name = "displayStudentAttendence";
            this.Text = "displayStudentAttendence";
            this.Load += new System.EventHandler(this.displayStudentAttendence_Load);
            ((System.ComponentModel.ISupportInitialize)(this.displayAss)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView displayAss;
    }
}