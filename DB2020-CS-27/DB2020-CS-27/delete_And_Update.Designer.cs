﻿
namespace DB2020_CS_27
{
    partial class delete_And_Update
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.studentID = new System.Windows.Forms.Label();
            this.getRecord = new System.Windows.Forms.Button();
            this.deleteStudent = new System.Windows.Forms.Button();
            this.updateStudent = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.registrationNumber = new System.Windows.Forms.Label();
            this.value = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(227, 190);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 0;
            // 
            // studentID
            // 
            this.studentID.AutoSize = true;
            this.studentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentID.Location = new System.Drawing.Point(98, 50);
            this.studentID.Name = "studentID";
            this.studentID.Size = new System.Drawing.Size(104, 25);
            this.studentID.TabIndex = 1;
            this.studentID.Text = "Student ID";
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(66, 105);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(150, 53);
            this.getRecord.TabIndex = 3;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // deleteStudent
            // 
            this.deleteStudent.Location = new System.Drawing.Point(844, 422);
            this.deleteStudent.Name = "deleteStudent";
            this.deleteStudent.Size = new System.Drawing.Size(150, 53);
            this.deleteStudent.TabIndex = 4;
            this.deleteStudent.Text = "Delete Student";
            this.deleteStudent.UseVisualStyleBackColor = true;
            this.deleteStudent.Click += new System.EventHandler(this.deleteStudent_Click);
            // 
            // updateStudent
            // 
            this.updateStudent.Location = new System.Drawing.Point(643, 422);
            this.updateStudent.Name = "updateStudent";
            this.updateStudent.Size = new System.Drawing.Size(150, 53);
            this.updateStudent.TabIndex = 5;
            this.updateStudent.Text = "Update Student";
            this.updateStudent.UseVisualStyleBackColor = true;
            this.updateStudent.Click += new System.EventHandler(this.updateStudent_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(251, 51);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 19;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // registrationNumber
            // 
            this.registrationNumber.AutoSize = true;
            this.registrationNumber.Location = new System.Drawing.Point(888, 54);
            this.registrationNumber.Name = "registrationNumber";
            this.registrationNumber.Size = new System.Drawing.Size(138, 17);
            this.registrationNumber.TabIndex = 20;
            this.registrationNumber.Text = "Registration Number";
            // 
            // value
            // 
            this.value.AutoSize = true;
            this.value.Location = new System.Drawing.Point(1040, 54);
            this.value.Name = "value";
            this.value.Size = new System.Drawing.Size(46, 17);
            this.value.TabIndex = 21;
            this.value.Text = "label2";
            // 
            // delete_And_Update
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1229, 512);
            this.Controls.Add(this.value);
            this.Controls.Add(this.registrationNumber);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateStudent);
            this.Controls.Add(this.deleteStudent);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.studentID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "delete_And_Update";
            this.Text = "delete_And_Update";
            this.Load += new System.EventHandler(this.delete_And_Update_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.Label studentID;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Button deleteStudent;
        private System.Windows.Forms.Button updateStudent;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label registrationNumber;
        private System.Windows.Forms.Label value;
    }
}