﻿
namespace DB2020_CS_27
{
    partial class updateAssessmentComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateAssessment = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.assessmentComponentName = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.searchIDCombo = new System.Windows.Forms.ComboBox();
            this.deleteAssessmentComponent = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // updateAssessment
            // 
            this.updateAssessment.Location = new System.Drawing.Point(440, 364);
            this.updateAssessment.Name = "updateAssessment";
            this.updateAssessment.Size = new System.Drawing.Size(150, 53);
            this.updateAssessment.TabIndex = 17;
            this.updateAssessment.Text = "Update Assessment";
            this.updateAssessment.UseVisualStyleBackColor = true;
            this.updateAssessment.Click += new System.EventHandler(this.updateAssessment_Click);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(35, 84);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 15;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // assessmentComponentName
            // 
            this.assessmentComponentName.AutoSize = true;
            this.assessmentComponentName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentComponentName.Location = new System.Drawing.Point(30, 27);
            this.assessmentComponentName.Name = "assessmentComponentName";
            this.assessmentComponentName.Size = new System.Drawing.Size(251, 25);
            this.assessmentComponentName.TabIndex = 13;
            this.assessmentComponentName.Text = "Assessment Component ID";
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(120, 176);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 12;
            this.update_And_Delete_Table.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.update_And_Delete_Table_DataError);
            // 
            // searchIDCombo
            // 
            this.searchIDCombo.FormattingEnabled = true;
            this.searchIDCombo.Location = new System.Drawing.Point(404, 31);
            this.searchIDCombo.Name = "searchIDCombo";
            this.searchIDCombo.Size = new System.Drawing.Size(366, 24);
            this.searchIDCombo.TabIndex = 18;
            this.searchIDCombo.SelectedIndexChanged += new System.EventHandler(this.searchID_SelectedIndexChanged);
            this.searchIDCombo.SelectedValueChanged += new System.EventHandler(this.searchID_SelectedValueChanged);
            // 
            // deleteAssessmentComponent
            // 
            this.deleteAssessmentComponent.Location = new System.Drawing.Point(605, 364);
            this.deleteAssessmentComponent.Name = "deleteAssessmentComponent";
            this.deleteAssessmentComponent.Size = new System.Drawing.Size(150, 53);
            this.deleteAssessmentComponent.TabIndex = 19;
            this.deleteAssessmentComponent.Text = "Delete Assessment Component";
            this.deleteAssessmentComponent.UseVisualStyleBackColor = true;
            this.deleteAssessmentComponent.Click += new System.EventHandler(this.deleteAssessmentComponent_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(823, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 25);
            this.label1.TabIndex = 20;
            this.label1.Text = "Assessment Component Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(1139, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 25);
            this.label2.TabIndex = 21;
            this.label2.Text = "lable";
            // 
            // updateAssessmentComponent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.deleteAssessmentComponent);
            this.Controls.Add(this.searchIDCombo);
            this.Controls.Add(this.updateAssessment);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.assessmentComponentName);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "updateAssessmentComponent";
            this.Text = "updateAssessmentComponent";
            this.Load += new System.EventHandler(this.updateAssessmentComponent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateAssessment;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label assessmentComponentName;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.ComboBox searchIDCombo;
        private System.Windows.Forms.Button deleteAssessmentComponent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}