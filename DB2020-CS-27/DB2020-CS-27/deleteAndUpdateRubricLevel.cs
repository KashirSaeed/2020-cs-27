﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{
    public partial class deleteAndUpdateRubricLevel : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        public deleteAndUpdateRubricLevel()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        private void deleteAndUpdateRubricLevel_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT Id FROM RubricLevel   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();
        }
        string nums;
        private void getRecord_Click(object sender, EventArgs e)
        {
            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM RubricLevel WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(nums));

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");
        }
        string tempCloid;
        int flag = 0;
        int flag1 = 0;
        private void updateAssessment_Click(object sender, EventArgs e)
        {

            flag1 = 0;
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("    SELECT MeasurementLevel  FROM RubricLevel   WHERE RubricId = @searchID      ", con1);
            cmd2.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            SqlDataReader myReader = cmd2.ExecuteReader();
            while (myReader.Read())
            {
                if (update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString() == myReader.GetValue(0).ToString())
                {
                    flag1 = 1;
                    break;
                }

                //MessageBox.Show( myReader.GetValue(0).ToString() );
                //comboBox2.Items.Add(myReader[0]);
                //totalAssessmentMarks = int.Parse(myReader["TotalMarks"].ToString());
            }
            myReader.Close();
            //MessageBox.Show(totalAssessmentMarks);
            cmd2.ExecuteNonQuery();
            //MessageBox.Show("AssessmentComponenet Added Successfully ");


            if (flag1 == 1)
            {
                MessageBox.Show(" this measurement level already exist. Please enter another measurement level!!!!!!!!!!!! ");
            }
            else
            {
                tempCloid = update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString();
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd1 = new SqlCommand("  SELECT Id FROM Rubric   ", con);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(tempCloid));
                SqlDataReader DR1 = cmd1.ExecuteReader();

                while (DR1.Read())
                {
                    flag = 0;
                    //MessageBox.Show("kashir saeed");
                    if (DR1.GetValue(0).ToString() == tempCloid)
                    {
                        warningLable.Hide();
                        if (regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString()) && regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString()) && regex2.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString()))
                        {
                            flag = 1;
                            DR1.Close();

                            //var con = Configuration.getInstance().getConnection();
                            SqlCommand cmd = new SqlCommand(" UPDATE RubricLevel SET RubricId = @studentID , Details = @studentName , MeasurementLevel = @measurnmenLevel  where  Id = @searchID  ", con);
                            cmd.Parameters.AddWithValue("@studentID", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@studentName", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@measurnmenLevel", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                            cmd.Parameters.AddWithValue("@searchID", int.Parse(nums));


                            cmd.ExecuteNonQuery();
                            MessageBox.Show("Successfully updated");
                            break;

                        }
                        else
                        {
                            MessageBox.Show("please update in correct format");
                        }

                    }
                    else
                    {
                        warningLable.Show();
                    }
                }
                if (flag != 1)
                {
                    DR1.Close();

                }
            }












            


        }

        private void update_And_Delete_Table_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.Cancel = true;
            MessageBox.Show("Any of the previously cell is edited incorrectly");
        }

        private void deleteAssessment_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd4 = new SqlCommand(" DELETE from StudentResult where  RubricMeasurementId = @searchID  ", con);
            cmd4.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd4.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");


            SqlCommand cmd3 = new SqlCommand(" DELETE from RubricLevel where  Id = @searchID  ", con);
            cmd3.Parameters.AddWithValue("@searchID", int.Parse(nums));
            cmd3.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }
        int cbi;
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {

                cbi = (int)comboBox1.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Details  FROM RubricLevel WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label2.Text = myReader["Details"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }
    }
}
