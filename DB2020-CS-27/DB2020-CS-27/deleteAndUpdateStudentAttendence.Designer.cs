﻿
namespace DB2020_CS_27
{
    partial class deleteAndUpdateStudentAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.studentID = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.updateAssessment = new System.Windows.Forms.Button();
            this.deleteAssessment = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.attendenceID = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.warningLable = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(286, 102);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(366, 24);
            this.comboBox2.TabIndex = 54;
            this.comboBox2.SelectedValueChanged += new System.EventHandler(this.comboBox2_SelectedValueChanged);
            // 
            // studentID
            // 
            this.studentID.AutoSize = true;
            this.studentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentID.Location = new System.Drawing.Point(30, 102);
            this.studentID.Name = "studentID";
            this.studentID.Size = new System.Drawing.Size(104, 25);
            this.studentID.TabIndex = 53;
            this.studentID.Text = "Student ID";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(286, 54);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 52;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // updateAssessment
            // 
            this.updateAssessment.Location = new System.Drawing.Point(464, 525);
            this.updateAssessment.Name = "updateAssessment";
            this.updateAssessment.Size = new System.Drawing.Size(150, 53);
            this.updateAssessment.TabIndex = 51;
            this.updateAssessment.Text = "Update Assessment";
            this.updateAssessment.UseVisualStyleBackColor = true;
            this.updateAssessment.Click += new System.EventHandler(this.updateAssessment_Click);
            // 
            // deleteAssessment
            // 
            this.deleteAssessment.Location = new System.Drawing.Point(620, 525);
            this.deleteAssessment.Name = "deleteAssessment";
            this.deleteAssessment.Size = new System.Drawing.Size(150, 53);
            this.deleteAssessment.TabIndex = 50;
            this.deleteAssessment.Text = "Delete Assessment";
            this.deleteAssessment.UseVisualStyleBackColor = true;
            this.deleteAssessment.Click += new System.EventHandler(this.deleteAssessment_Click_1);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(35, 203);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 49;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // attendenceID
            // 
            this.attendenceID.AutoSize = true;
            this.attendenceID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.attendenceID.Location = new System.Drawing.Point(30, 54);
            this.attendenceID.Name = "attendenceID";
            this.attendenceID.Size = new System.Drawing.Size(136, 25);
            this.attendenceID.TabIndex = 48;
            this.attendenceID.Text = "Attendence ID";
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(120, 282);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 47;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(769, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 25);
            this.label1.TabIndex = 56;
            this.label1.Text = "Registration Number";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(769, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 25);
            this.label2.TabIndex = 55;
            this.label2.Text = "Attendence Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1030, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 25);
            this.label3.TabIndex = 58;
            this.label3.Text = "lable";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1030, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 25);
            this.label4.TabIndex = 57;
            this.label4.Text = "lable";
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.BackColor = System.Drawing.SystemColors.Control;
            this.warningLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(234, 464);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(481, 25);
            this.warningLable.TabIndex = 59;
            this.warningLable.Text = "Attendance Status should be in the range  from 1  to  4";
            // 
            // deleteAndUpdateStudentAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1254, 633);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.studentID);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateAssessment);
            this.Controls.Add(this.deleteAssessment);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.attendenceID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "deleteAndUpdateStudentAttendence";
            this.Text = "deleteAndUpdateStudentAttendence";
            this.Load += new System.EventHandler(this.deleteAndUpdateStudentAttendence_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label studentID;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button updateAssessment;
        private System.Windows.Forms.Button deleteAssessment;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label attendenceID;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label warningLable;
    }
}