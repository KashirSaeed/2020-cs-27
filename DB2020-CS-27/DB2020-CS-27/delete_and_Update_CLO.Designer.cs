﻿
namespace DB2020_CS_27
{
    partial class delete_and_Update_CLO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateCLO = new System.Windows.Forms.Button();
            this.deleteCLO = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.cloID = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.value = new System.Windows.Forms.Label();
            this.cloName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // updateCLO
            // 
            this.updateCLO.Location = new System.Drawing.Point(464, 367);
            this.updateCLO.Name = "updateCLO";
            this.updateCLO.Size = new System.Drawing.Size(150, 53);
            this.updateCLO.TabIndex = 17;
            this.updateCLO.Text = "Update CLO";
            this.updateCLO.UseVisualStyleBackColor = true;
            this.updateCLO.Click += new System.EventHandler(this.updateCLO_Click);
            // 
            // deleteCLO
            // 
            this.deleteCLO.Location = new System.Drawing.Point(620, 367);
            this.deleteCLO.Name = "deleteCLO";
            this.deleteCLO.Size = new System.Drawing.Size(150, 53);
            this.deleteCLO.TabIndex = 16;
            this.deleteCLO.Text = "Delete CLO";
            this.deleteCLO.UseVisualStyleBackColor = true;
            this.deleteCLO.Click += new System.EventHandler(this.deleteCLO_Click);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(35, 84);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 15;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // cloID
            // 
            this.cloID.AutoSize = true;
            this.cloID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cloID.Location = new System.Drawing.Point(30, 31);
            this.cloID.Name = "cloID";
            this.cloID.Size = new System.Drawing.Size(78, 25);
            this.cloID.TabIndex = 13;
            this.cloID.Text = "CLO ID";
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(120, 176);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 12;
            this.update_And_Delete_Table.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.update_And_Delete_Table_DataError);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(248, 31);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 19;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // value
            // 
            this.value.AutoSize = true;
            this.value.Location = new System.Drawing.Point(807, 34);
            this.value.Name = "value";
            this.value.Size = new System.Drawing.Size(46, 17);
            this.value.TabIndex = 25;
            this.value.Text = "label2";
            // 
            // cloName
            // 
            this.cloName.AutoSize = true;
            this.cloName.Location = new System.Drawing.Point(725, 34);
            this.cloName.Name = "cloName";
            this.cloName.Size = new System.Drawing.Size(85, 17);
            this.cloName.TabIndex = 24;
            this.cloName.Text = "CLO Name: ";
            // 
            // delete_and_Update_CLO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 450);
            this.Controls.Add(this.value);
            this.Controls.Add(this.cloName);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateCLO);
            this.Controls.Add(this.deleteCLO);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.cloID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "delete_and_Update_CLO";
            this.Text = "delete_and_Update_CLO";
            this.Load += new System.EventHandler(this.delete_and_Update_CLO_Load);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateCLO;
        private System.Windows.Forms.Button deleteCLO;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label cloID;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label value;
        private System.Windows.Forms.Label cloName;
    }
}