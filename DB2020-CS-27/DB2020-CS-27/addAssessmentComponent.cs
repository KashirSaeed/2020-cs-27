﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace DB2020_CS_27
{
    public partial class addAssessmentComponent : Form
    {
        Regex regex1 = new Regex("^[a-zA-z]*$");
        Regex regex2 = new Regex("^[0-9]*$");
        Regex regex3 = new Regex("^[a-zA-z0-9]*$");
        public addAssessmentComponent()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        private void addAssessmentComponent_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT  Id  FROM Rubric   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                rubricIDCombobox.Items.Add(DR1[0]);
            }
            DR1.Close();


            SqlCommand cmd2 = new SqlCommand("  SELECT  Id  FROM Assessment   ", con);
            SqlDataReader DR2 = cmd2.ExecuteReader();

            while (DR2.Read())
            {
                assessmentIDCombobox.Items.Add(DR2[0]);
            }
            DR2.Close();
        }
        string nums;
        string nums1;
        int totalAssessmentMarks;
        int marks=0;
        int tempMarks=0;
        int sum =0;

        private void addAssessComponent_Click(object sender, EventArgs e)
        {
            marks = 0;
            sum = 0;
            tempMarks = 0;
            //temp = assessmentIDCombobox.Text;

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("    SELECT TotalMarks  FROM Assessment   WHERE Id = @searchID      ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(assessmentIDCombobox.Text));
            SqlDataReader myReader = cmd1.ExecuteReader();
            while (myReader.Read())
            {
                //comboBox2.Items.Add(myReader[0]);
                totalAssessmentMarks = int.Parse(myReader["TotalMarks"].ToString());
            }
            myReader.Close();
            //MessageBox.Show(totalAssessmentMarks);
            cmd1.ExecuteNonQuery();
            MessageBox.Show("AssessmentComponenet Added Successfully ");








            SqlCommand cmd2 = new SqlCommand("    SELECT TotalMarks  FROM AssessmentComponent  WHERE AssessmentId =   @searchID   ", con1);
            cmd2.Parameters.AddWithValue("@searchID", int.Parse(assessmentIDCombobox.Text));
            SqlDataReader myReader1 = cmd2.ExecuteReader();
            while (myReader1.Read())
            {
                //comboBox2.Items.Add(myReader[0]);
                sum = int.Parse(myReader1["TotalMarks"].ToString());
                marks = marks + sum;
            }
            tempMarks = marks;
            marks = marks + int.Parse(totalMarksBox.Text);
            myReader1.Close();
            //MessageBox.Show(marks.ToString());
            cmd2.ExecuteNonQuery();
            MessageBox.Show("AssessmentComponenet Added Successfully ");

            if( marks <= totalAssessmentMarks)
            {
                if (regex1.IsMatch(measurmentLevelTextbox.Text) && regex2.IsMatch(totalMarksBox.Text))
                {
                    if (measurmentLevelTextbox.Text == "" || rubricIDCombobox.Text == "" || totalMarksBox.Text == "" || dateTimePicker1.Text == "" || dateTimePicker2.Text == "" || assessmentIDCombobox.Text == "")
                    {
                        warningLable.Show();
                    }
                    else
                    {
                        warningLable.Hide();

                        nums = new String(rubricIDCombobox.Text.Where(Char.IsDigit).ToArray());
                        nums1 = new String(assessmentIDCombobox.Text.Where(Char.IsDigit).ToArray());
                        //var con1 = Configuration.getInstance().getConnection();
                        SqlCommand cmd = new SqlCommand(" insert into AssessmentComponent values(@firstName , @lastName , @contact , @email , @registrationNumber , @status   ) ", con1);
                        cmd.Parameters.AddWithValue("@firstName", measurmentLevelTextbox.Text);
                        cmd.Parameters.AddWithValue("@lastName", int.Parse(nums));
                        cmd.Parameters.AddWithValue("@contact", int.Parse(totalMarksBox.Text));
                        cmd.Parameters.AddWithValue("@email", DateTime.Parse(dateTimePicker1.Text));
                        cmd.Parameters.AddWithValue("@registrationNumber", DateTime.Parse(dateTimePicker2.Text));
                        cmd.Parameters.AddWithValue("@status", int.Parse(nums1));

                        cmd.ExecuteNonQuery();
                        MessageBox.Show("AssessmentComponenet Added Successfully ");



                    }
                }

                else
                {
                    MessageBox.Show(warningLable.Text);
                }
            }
            else
            {
                MessageBox.Show("you can add the the total marks of assessment component upto:    " +(totalAssessmentMarks-tempMarks).ToString());
            }








            

         
        }
        int cbi;
        private void rubricIDCombobox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (rubricIDCombobox.SelectedItem != null)
            {

                cbi = (int)rubricIDCombobox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Details  FROM Rubric WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label2.Text = myReader["Details"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }

        private void assessmentIDCombobox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (assessmentIDCombobox.SelectedItem != null)
            {

                cbi = (int)assessmentIDCombobox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Title  FROM Assessment WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label3.Text = myReader["Title"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }
    }
}
