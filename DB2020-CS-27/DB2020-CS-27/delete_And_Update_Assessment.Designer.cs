﻿
namespace DB2020_CS_27
{
    partial class delete_And_Update_Assessment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateAssessment = new System.Windows.Forms.Button();
            this.deleteAssessment = new System.Windows.Forms.Button();
            this.getRecord = new System.Windows.Forms.Button();
            this.assessmentID = new System.Windows.Forms.Label();
            this.update_And_Delete_Table = new System.Windows.Forms.DataGridView();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.value = new System.Windows.Forms.Label();
            this.tittle = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // updateAssessment
            // 
            this.updateAssessment.Location = new System.Drawing.Point(467, 363);
            this.updateAssessment.Name = "updateAssessment";
            this.updateAssessment.Size = new System.Drawing.Size(150, 53);
            this.updateAssessment.TabIndex = 11;
            this.updateAssessment.Text = "Update Assessment";
            this.updateAssessment.UseVisualStyleBackColor = true;
            this.updateAssessment.Click += new System.EventHandler(this.updateStudent_Click);
            // 
            // deleteAssessment
            // 
            this.deleteAssessment.Location = new System.Drawing.Point(623, 363);
            this.deleteAssessment.Name = "deleteAssessment";
            this.deleteAssessment.Size = new System.Drawing.Size(150, 53);
            this.deleteAssessment.TabIndex = 10;
            this.deleteAssessment.Text = "Delete Assessment";
            this.deleteAssessment.UseVisualStyleBackColor = true;
            this.deleteAssessment.Click += new System.EventHandler(this.deleteStudent_Click);
            // 
            // getRecord
            // 
            this.getRecord.Location = new System.Drawing.Point(38, 79);
            this.getRecord.Name = "getRecord";
            this.getRecord.Size = new System.Drawing.Size(125, 53);
            this.getRecord.TabIndex = 9;
            this.getRecord.Text = "Get Record";
            this.getRecord.UseVisualStyleBackColor = true;
            this.getRecord.Click += new System.EventHandler(this.getRecord_Click);
            // 
            // assessmentID
            // 
            this.assessmentID.AutoSize = true;
            this.assessmentID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessmentID.Location = new System.Drawing.Point(33, 27);
            this.assessmentID.Name = "assessmentID";
            this.assessmentID.Size = new System.Drawing.Size(144, 25);
            this.assessmentID.TabIndex = 7;
            this.assessmentID.Text = "Assessment ID";
            // 
            // update_And_Delete_Table
            // 
            this.update_And_Delete_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.update_And_Delete_Table.Location = new System.Drawing.Point(123, 172);
            this.update_And_Delete_Table.Name = "update_And_Delete_Table";
            this.update_And_Delete_Table.RowHeadersWidth = 51;
            this.update_And_Delete_Table.RowTemplate.Height = 24;
            this.update_And_Delete_Table.Size = new System.Drawing.Size(650, 150);
            this.update_And_Delete_Table.TabIndex = 6;
            this.update_And_Delete_Table.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.update_And_Delete_Table_DataError);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(289, 27);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(366, 24);
            this.comboBox1.TabIndex = 20;
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // value
            // 
            this.value.AutoSize = true;
            this.value.Location = new System.Drawing.Point(800, 30);
            this.value.Name = "value";
            this.value.Size = new System.Drawing.Size(46, 17);
            this.value.TabIndex = 23;
            this.value.Text = "label2";
            // 
            // tittle
            // 
            this.tittle.AutoSize = true;
            this.tittle.Location = new System.Drawing.Point(718, 30);
            this.tittle.Name = "tittle";
            this.tittle.Size = new System.Drawing.Size(39, 17);
            this.tittle.TabIndex = 22;
            this.tittle.Text = "Tittle";
            // 
            // delete_And_Update_Assessment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 450);
            this.Controls.Add(this.value);
            this.Controls.Add(this.tittle);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.updateAssessment);
            this.Controls.Add(this.deleteAssessment);
            this.Controls.Add(this.getRecord);
            this.Controls.Add(this.assessmentID);
            this.Controls.Add(this.update_And_Delete_Table);
            this.Name = "delete_And_Update_Assessment";
            this.Text = "delete_And_Update_Assessment";
            this.Load += new System.EventHandler(this.delete_And_Update_Assessment_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.update_And_Delete_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateAssessment;
        private System.Windows.Forms.Button deleteAssessment;
        private System.Windows.Forms.Button getRecord;
        private System.Windows.Forms.Label assessmentID;
        private System.Windows.Forms.DataGridView update_And_Delete_Table;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label value;
        private System.Windows.Forms.Label tittle;
    }
}