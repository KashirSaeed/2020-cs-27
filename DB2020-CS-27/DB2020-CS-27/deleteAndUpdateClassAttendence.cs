﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace DB2020_CS_27
{
    public partial class deleteAndUpdateClassAttendence : Form
    {
        public deleteAndUpdateClassAttendence()
        {
            InitializeComponent();
        }

        string nums;
        private void getRecord_Click(object sender, EventArgs e)
        {
           

            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM ClassAttendance WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");
        }

        private void updateAttendenceDate_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand(" UPDATE ClassAttendance SET AttendanceDate = @updateDate    where  Id = @searchID  ", con);
            cmd.Parameters.AddWithValue("@updateDate", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));


            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully updated");
        }

        private void deleteAttendenceDate_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd1 = new SqlCommand(" DELETE from StudentAttendance where AttendanceId = @searchID  ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd1.ExecuteNonQuery();

            SqlCommand cmd = new SqlCommand(" DELETE from ClassAttendance where Id = @searchID  ", con);
            cmd.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }

        private void deleteAndUpdateClassAttendence_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT Id FROM ClassAttendance   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT AttendanceDate FROM ClassAttendance WHERE Id =@searchID ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            SqlDataReader sdr = cmd1.ExecuteReader();
            sdr.Read();
            value.Text = sdr["AttendanceDate"].ToString();
            sdr.Close();
        }
    }
}
