﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace DB2020_CS_27
{

    public partial class delete_and_Update_CLO : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");

        public delete_and_Update_CLO()
        {
            InitializeComponent();
        }

        string nums;
        private void getRecord_Click(object sender, EventArgs e)
        {

         


            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());

            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT * FROM Clo WHERE Id =@searchID ", con1);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));

            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            update_And_Delete_Table.DataSource = dt;

            update_And_Delete_Table.Columns["Id"].ReadOnly = true;
            MessageBox.Show("Successfully displayed");

        }

        private void updateCLO_Click(object sender, EventArgs e)
        {
            if (regex1.IsMatch(update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString()) )
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand(" UPDATE Clo SET Name = @nameCLO  ,DateCreated = @dateCreated , DateUpdated = @dateUpdated  where  Id = @searchID  ", con);
                cmd.Parameters.AddWithValue("@nameCLO", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[1].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@dateCreated", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[2].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@dateUpdated", update_And_Delete_Table.Rows[update_And_Delete_Table.CurrentRow.Index].Cells[3].FormattedValue.ToString());
                cmd.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));


                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully updated");
            }
            else
            {
                MessageBox.Show( "Please edit in correct format" );
            }


             
        }

        private void deleteCLO_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            List<int> tempList = new List<int>();
            List<int> tempList1 = new List<int>();
            List<int> tempList2 = new List<int>();

            nums = new String(comboBox1.Text.Where(Char.IsDigit).ToArray());
            SqlCommand cmd3 = new SqlCommand("SELECT Id  FROM Rubric where CloId = @searchID", con);
            cmd3.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            SqlDataReader Sdr = cmd3.ExecuteReader();
            int idx = 0;
            while (Sdr.Read())
            {
                tempList.Add(int.Parse(Sdr.GetValue(0).ToString()));
            }
            Sdr.Close();

            for( int i = 0; i < tempList.Count; i++)
            {
                SqlCommand cmd6 = new SqlCommand("SELECT Id  FROM AssessmentComponent where RubricId = @searchID", con);
                cmd6.Parameters.AddWithValue("@searchID", int.Parse(tempList[i].ToString()));
                SqlDataReader Sdr2 = cmd6.ExecuteReader();
                while (Sdr2.Read())
                {
                    tempList1.Add(int.Parse(Sdr2.GetValue(0).ToString()));
                }
                Sdr2.Close();
            }

            for (int i = 0; i < tempList.Count; i++)
            {
                SqlCommand cmd10 = new SqlCommand("SELECT Id  FROM RubricLevel where RubricId = @searchID", con);
                cmd10.Parameters.AddWithValue("@searchID", int.Parse(tempList[i].ToString()));
                SqlDataReader Sdr4 = cmd10.ExecuteReader();
                while (Sdr4.Read())
                {
                    tempList2.Add(int.Parse(Sdr4.GetValue(0).ToString()));
                }
                Sdr4.Close();
            }

            for (int i = 0; i < tempList1.Count; i++)
            {

                SqlCommand cmd9 = new SqlCommand(" DELETE from StudentResult where  AssessmentComponentId = @searchid ", con);
                cmd9.Parameters.AddWithValue("@searchid", int.Parse(tempList1[i].ToString()));
                cmd9.ExecuteNonQuery();
            }

            for(int l = 0; l < tempList2.Count; l++)
            {
                SqlCommand cmd9 = new SqlCommand(" DELETE from StudentResult where  RubricMeasurementId = @searchid ", con);
                cmd9.Parameters.AddWithValue("@searchid", int.Parse(tempList2[l].ToString()));
                cmd9.ExecuteNonQuery();
            }


            for (int j = 0; j < tempList1.Count; j++)
            {
                //MessageBox.Show(tempList1[i].ToString());
                SqlCommand cmd4 = new SqlCommand(" DELETE from AssessmentComponent where  Id = @searchid ", con);
                cmd4.Parameters.AddWithValue("@searchid", int.Parse(tempList1[j].ToString()));
                cmd4.ExecuteNonQuery();
            }

            for(int k = 0; k < tempList2.Count; k++)
            {
                SqlCommand cmd11 = new SqlCommand(" DELETE from RubricLevel where  Id = @searchid ", con);
                cmd11.Parameters.AddWithValue("@searchid", int.Parse(tempList2[k].ToString()));
                cmd11.ExecuteNonQuery();
            }

            for (int i = 0; i < tempList.Count; i++)
            {
                SqlCommand cmd4 = new SqlCommand(" DELETE from AssessmentComponent where  RubricId = @searchid ", con);
                cmd4.Parameters.AddWithValue("@searchid", int.Parse(tempList[i].ToString()));
                cmd4.ExecuteNonQuery();
            }




            SqlCommand cmd1 = new SqlCommand(" DELETE from Rubric where CloId = @searchID  ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd1.ExecuteNonQuery();



            SqlCommand cmd = new SqlCommand(" DELETE from Clo where Id = @searchID  ", con);
            cmd.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");


        }

        private void delete_and_Update_CLO_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT  Id FROM Clo   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                comboBox1.Items.Add(DR1[0]);

            }
            DR1.Close();
        }

        private void update_And_Delete_Table_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Any of the previuosly cell is edited incorrectly");
            e.Cancel = true;
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("SELECT Name FROM Clo WHERE Id =@searchID ", con);
            cmd1.Parameters.AddWithValue("@searchID", int.Parse(comboBox1.Text));
            SqlDataReader sdr = cmd1.ExecuteReader();
            sdr.Read();
            value.Text = sdr["Name"].ToString();
            sdr.Close();
        }
    }
}
