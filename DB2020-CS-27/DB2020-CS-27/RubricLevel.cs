﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;


namespace DB2020_CS_27
{
    public partial class RubricLevel : Form
    {
        Regex regex1 = new Regex("^[a-z A-z]*$");
        Regex regex3 = new Regex("^[0-9]*$");
        public RubricLevel()
        {
            InitializeComponent();
            warningLable.Hide();
        }

        private void RubricLevel_Load(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("  SELECT Id FROM Rubric   ", con);
            SqlDataReader DR1 = cmd.ExecuteReader();

            while (DR1.Read())
            {
                rubricidComboBox.Items.Add(DR1[0]);
            }
            DR1.Close();
        }

        private void rubDetail_TextChanged(object sender, EventArgs e)
        {

        }
        string nums;
        int flag = 0;
        private void addRubricLevel_Click(object sender, EventArgs e)
        {
            flag = 0;
            var con1 = Configuration.getInstance().getConnection();
            SqlCommand cmd2 = new SqlCommand("    SELECT MeasurementLevel  FROM RubricLevel   WHERE RubricId = @searchID      ", con1);
            cmd2.Parameters.AddWithValue("@searchID", int.Parse(rubricidComboBox.Text));
            SqlDataReader myReader = cmd2.ExecuteReader();
            while (myReader.Read())
            {
                if(measurmentLevelTextbox.Text == myReader.GetValue(0).ToString())
                {
                    flag = 1;
                    break;
                }

                    //MessageBox.Show( myReader.GetValue(0).ToString() );
                //comboBox2.Items.Add(myReader[0]);
                //totalAssessmentMarks = int.Parse(myReader["TotalMarks"].ToString());
            }
            myReader.Close();
            //MessageBox.Show(totalAssessmentMarks);
            cmd2.ExecuteNonQuery();
            //MessageBox.Show("AssessmentComponenet Added Successfully ");
            if (flag == 1)
            {
                MessageBox.Show(" this measurement level already exist. Please enter another measurement level!!!!!!!!!!!! ");
            }
            else
            {
                if (regex3.IsMatch(measurmentLevelTextbox.Text) && regex1.IsMatch(rubDetail.Text))
                {

                    if (rubricidComboBox.Text == "" || rubDetail.Text == "" || measurmentLevelTextbox.Text == "")
                    {
                        warningLable.Show();
                    }
                    else
                    {
                        warningLable.Hide();
                        nums = new String(rubricidComboBox.Text.Where(Char.IsDigit).ToArray());
                        //MessageBox.Show(nums);
                        //var con1 = Configuration.getInstance().getConnection();
                        SqlCommand cmd1 = new SqlCommand("insert into RubricLevel values(@rubricidComboBox ,@rubDetail, @measurmentLevelTextbox   ) ", con1);
                        cmd1.Parameters.AddWithValue("@rubricidComboBox", int.Parse(nums));
                        cmd1.Parameters.AddWithValue("@rubDetail", rubDetail.Text);
                        cmd1.Parameters.AddWithValue("@measurmentLevelTextbox", int.Parse(measurmentLevelTextbox.Text));
                        cmd1.ExecuteNonQuery();
                        MessageBox.Show("rubric level Added Successfully ");
                    }
                }

                else
                {
                    MessageBox.Show(warningLable.Text);
                }
            }












            
        }
        int cbi;
        private void rubricidComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (rubricidComboBox.SelectedItem != null)
            {

                cbi = (int)rubricidComboBox.SelectedItem;
                var con1 = Configuration.getInstance().getConnection();
                SqlDataReader myReader = null;
                SqlCommand cmd1 = new SqlCommand("SELECT Details  FROM Rubric WHERE Id = @searchID ", con1);
                cmd1.Parameters.AddWithValue("@searchID", int.Parse(cbi.ToString()));
                myReader = cmd1.ExecuteReader();
                while (myReader.Read())
                {
                    label2.Text = myReader["Details"].ToString();

                }
                myReader.Close();
                //loadindStudentID();

            }
        }

        private void rubricidComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
