﻿
namespace DB2020_CS_27
{
    partial class Rubric
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cloID = new System.Windows.Forms.Label();
            this.rubricDetail = new System.Windows.Forms.Label();
            this.cloidComboBox = new System.Windows.Forms.ComboBox();
            this.rubDetail = new System.Windows.Forms.RichTextBox();
            this.addRubric = new System.Windows.Forms.Button();
            this.rubricID = new System.Windows.Forms.Label();
            this.rubricIDTextbox = new System.Windows.Forms.TextBox();
            this.warningLable = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cloID
            // 
            this.cloID.AutoSize = true;
            this.cloID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cloID.Location = new System.Drawing.Point(126, 109);
            this.cloID.Name = "cloID";
            this.cloID.Size = new System.Drawing.Size(91, 29);
            this.cloID.TabIndex = 0;
            this.cloID.Text = "CLO ID";
            // 
            // rubricDetail
            // 
            this.rubricDetail.AutoSize = true;
            this.rubricDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricDetail.Location = new System.Drawing.Point(126, 198);
            this.rubricDetail.Name = "rubricDetail";
            this.rubricDetail.Size = new System.Drawing.Size(75, 29);
            this.rubricDetail.TabIndex = 1;
            this.rubricDetail.Text = "Detail";
            // 
            // cloidComboBox
            // 
            this.cloidComboBox.FormattingEnabled = true;
            this.cloidComboBox.Location = new System.Drawing.Point(278, 106);
            this.cloidComboBox.Name = "cloidComboBox";
            this.cloidComboBox.Size = new System.Drawing.Size(352, 24);
            this.cloidComboBox.TabIndex = 2;
            this.cloidComboBox.SelectedIndexChanged += new System.EventHandler(this.cloidComboBox_SelectedIndexChanged);
            this.cloidComboBox.SelectedValueChanged += new System.EventHandler(this.cloidComboBox_SelectedValueChanged);
            // 
            // rubDetail
            // 
            this.rubDetail.Location = new System.Drawing.Point(278, 172);
            this.rubDetail.Name = "rubDetail";
            this.rubDetail.Size = new System.Drawing.Size(352, 96);
            this.rubDetail.TabIndex = 3;
            this.rubDetail.Text = "";
            // 
            // addRubric
            // 
            this.addRubric.Location = new System.Drawing.Point(512, 329);
            this.addRubric.Name = "addRubric";
            this.addRubric.Size = new System.Drawing.Size(118, 43);
            this.addRubric.TabIndex = 4;
            this.addRubric.Text = "Add Rubric";
            this.addRubric.UseVisualStyleBackColor = true;
            this.addRubric.Click += new System.EventHandler(this.addRubric_Click);
            // 
            // rubricID
            // 
            this.rubricID.AutoSize = true;
            this.rubricID.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rubricID.Location = new System.Drawing.Point(126, 39);
            this.rubricID.Name = "rubricID";
            this.rubricID.Size = new System.Drawing.Size(112, 29);
            this.rubricID.TabIndex = 5;
            this.rubricID.Text = "Rubric ID";
            // 
            // rubricIDTextbox
            // 
            this.rubricIDTextbox.Location = new System.Drawing.Point(278, 39);
            this.rubricIDTextbox.Name = "rubricIDTextbox";
            this.rubricIDTextbox.Size = new System.Drawing.Size(352, 22);
            this.rubricIDTextbox.TabIndex = 6;
            // 
            // warningLable
            // 
            this.warningLable.AutoSize = true;
            this.warningLable.ForeColor = System.Drawing.Color.Red;
            this.warningLable.Location = new System.Drawing.Point(331, 288);
            this.warningLable.Name = "warningLable";
            this.warningLable.Size = new System.Drawing.Size(195, 17);
            this.warningLable.TabIndex = 22;
            this.warningLable.Text = "Please enter in correct format";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(739, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 29);
            this.label1.TabIndex = 23;
            this.label1.Text = "CLO Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(903, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 29);
            this.label2.TabIndex = 24;
            this.label2.Text = "CLO ID";
            // 
            // Rubric
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.warningLable);
            this.Controls.Add(this.rubricIDTextbox);
            this.Controls.Add(this.rubricID);
            this.Controls.Add(this.addRubric);
            this.Controls.Add(this.rubDetail);
            this.Controls.Add(this.cloidComboBox);
            this.Controls.Add(this.rubricDetail);
            this.Controls.Add(this.cloID);
            this.Name = "Rubric";
            this.Text = "Rubric";
            this.Load += new System.EventHandler(this.Rubric_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label cloID;
        private System.Windows.Forms.Label rubricDetail;
        private System.Windows.Forms.ComboBox cloidComboBox;
        private System.Windows.Forms.RichTextBox rubDetail;
        private System.Windows.Forms.Button addRubric;
        private System.Windows.Forms.Label rubricID;
        private System.Windows.Forms.TextBox rubricIDTextbox;
        private System.Windows.Forms.Label warningLable;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}